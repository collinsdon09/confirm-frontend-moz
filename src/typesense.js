import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";

const TYPESENSE_HOST = import.meta.env.VITE_APP_TYPESENSE_HOST;
const TYPESENSE_SEARCH_ONLY_KEY = import.meta.env.VITE_APP_TYPESENSE_SEARCH_ONLY_KEY;

export const instantSearchConfig = {
  server: {
    apiKey: TYPESENSE_SEARCH_ONLY_KEY,
    connectionTimeoutSeconds: 2,
    numRetries: 8,
    nodes: [
      {
        host: TYPESENSE_HOST, // For Typesense Cloud use xxx.a1.typesense.net
        port: 443, // For Typesense Cloud use 443
        protocol: "https", // For Typesense Cloud use https
      },
    ],
  },
  additionalSearchParameters: {
    // The following parameters are directly passed to Typesense's search API endpoint.
    //  So you can pass any parameters supported by the search endpoint below.
    //  queryBy is required.
    query_by: "name,description",
    /*     query_by_weights: "2,1",
    typo_tokens_threshold: 100,
    num_typos: 3,
    limit_hits: 150, */
  },
};

export const adapter = new TypesenseInstantSearchAdapter(instantSearchConfig);