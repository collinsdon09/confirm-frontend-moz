import { extendTheme } from "@chakra-ui/react";

const theme = {
    config:{
        intialColorMode:"dark",
        useSystemColorMode:true,

    },
    styles:{
        global:{
            body:{
                margin:0,
                "fontFamily": "Inter, system-ui, Avenir, Helvetica, Arial, sans-serif",
        
            },
            code:{
                "fontFamily":"source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace",
            },
        },
    },

};

export default extendTheme(theme)