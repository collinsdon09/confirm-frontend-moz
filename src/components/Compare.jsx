import React from 'react';
import CompareArrays from './CompareArrays';

const CompareArraysPage = () => {
  const array1 = [
    { name: 'John', age: 25 },
    { name: 'Jane', age: 30 },
    { name: 'Sam', age: 35 },
  ];

  const array2 = [
    { name: 'John', age: 25 },
    { name: 'Jane', age: 35 },
    { name: 'Sam', age: 35 },
  ];

  return (
    <div>
      <h2>Comparison Results:</h2>
      <CompareArrays array1={array1} array2={array2} />
    </div>
  );
};

export default CompareArraysPage;
