import { Heading, Text, Box, Flex } from "@chakra-ui/react";
import {
  Card,
  SimpleGrid,
  Button,
  CardHeader,
  CardBody,
  CardFooter,
} from "@chakra-ui/react";

import React, { useState, useEffect } from "react";

function ReceiptCard({ receiptData }) {
  const don = "DON";
  const [fetchedCheckedItems, setFetchedCheckedItems] = useState(0);
  const [receiptStatus, setReceiptStatus] = useState("OPEN");

  useEffect(() => {
    try {
      const checkedItemsCount = receiptData.checked_items.checkedItems.length;
      console.log("receiptData:", typeof receiptData);
      console.log();

      setFetchedCheckedItems(checkedItemsCount);

      if (checkedItemsCount === 0) {
        setReceiptStatus("OPEN");
      } else {
        setReceiptStatus("IN PROGRESS");
      }
    } catch (error) {
      console.log("Error:", error.message);
      setFetchedCheckedItems(0);
      setReceiptStatus("OPEN");
    }
  }, [receiptData]);

  const handleCardClick = () => {
    console.log("Card clicked");
  };

  return (
    <SimpleGrid
      spacing={14}
      templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
    >
      <Card>
        <CardHeader>
          <Heading size="md">
            {" "}
            Receipt Card:{receiptData.receipt_number}
          </Heading>
        </CardHeader>
        <CardBody>
          <Text>{don}</Text>
        </CardBody>
        <CardFooter>
          <Button onClick={() => handleCardClick(card)}>View here</Button>
        </CardFooter>
      </Card>
      {/* <Card>
        <CardHeader>
          <Heading size="md"> Customer dashboard</Heading>
        </CardHeader>
        <CardBody>
          <Text>View a summary of all your customers over the last month.</Text>
        </CardBody>
        <CardFooter>
          <Button>View here</Button>
        </CardFooter>
      </Card>
      <Card>
        <CardHeader>
          <Heading size="md"> Customer dashboard</Heading>
        </CardHeader>
        <CardBody>
          <Text>View a summary of all your customers over the last month.</Text>
        </CardBody>
        <CardFooter>
          <Button>View here</Button>
        </CardFooter>
      </Card> */}
    </SimpleGrid>
  );
}

export default ReceiptCard;
