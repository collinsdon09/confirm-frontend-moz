import { useState, useEffect } from "react";
import axios from "axios";

const Stats = ({ receipt_props }) => {
  const [cartSize, setCartSize] = useState(0);

  const fetchConfirmedReceiptsCart = async (receiptNumber) => {
    try {
      const response = await axios.get(
        `http://localhost:8000/api/get-confirmed-receipt_by_receipt_number/${receiptNumber}`
      );
      const data = response.data;
      //console.log("confi", response)
      //setCartSize(data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchConfirmedReceiptsCart(receipt_props.receipt_number);
  }, [receipt_props.receipt_number]);

  return (
    <div>
      {cartSize}
    </div>
  );
};

export default Stats;
