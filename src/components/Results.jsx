import { useState } from "react";
import { connectInfiniteHits } from "react-instantsearch-dom";
import {
  Card,
  SimpleGrid,
  Button,
  CardHeader,
  CardBody,
  CardFooter,
  Stack,
  Heading,
  Divider,
  ButtonGroup,
  Image,
  Text,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  HStack,
  VStack,
} from "@chakra-ui/react";
import axios from "axios";
import { grabOpenCardData } from "../functions/GrabCardByReceipt";
import _ from "lodash";

import {
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
} from '@chakra-ui/react'

import { useEffect } from "react";

const SearchResultsComponent = ({
  hits,
  hasmore,
  refinenext,
  refineprevious,
  receipt_data,
  updated,
  get,
}) => {
  const [quantity, setQuantity] = useState(1);
  const [selectedCardIndex, setSelectedCardIndex] = useState(-1);
  const [searchDuplicate, setSearchDuplicate] = useState("");
  const [responseDuplicate, setResponseDuplicate] = useState("");

  const [data, setData] = useState([]);

  var cartItems = [];

  useEffect(() => {
    // getCartItemsupdate();

    fetchData(receipt_data.receipt_number);
    // Use the handleClick function in the effect
    // get()
    // console.log('Child component mounted');

    // Cleanup function (optional)
    return () => {
      //  console.log('Child component unmounted');
    };
  }, []); // Include handleClick in the dependency array

  //console.log("RECEIPT NUMBER IN RESULTS", receipt_data.receipt_number)

  const getCartItemsupdate = async () => {
  //  console.log("get function called....");

    try {
      const response = await axios.get(
        `http://localhost:8000/get-receipt-stats/${receipt_data.receipt_number}`
      );

      // Do something with the response data

      console.log(
        "Updated Receipt from search results"
        //setCartItems(response.data.cart_items)
        // response.data.cart_items.splice(0, response.data.cart_items, ...groupedItems);
      );

      // response.data.cart_items.splice(0, response.data.cart_items.length, ...groupedItems);

      // setUpdatedReceiptData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  function checkNameExists(items, name) {
    for (let i = 0; i < items.length; i++) {
      if (items[i].item_name === name) {
        return true; // Name found in item_name property
      }
    }
    return false; // Name not found in any item_name property
  }

  const fetchData = (receipt_number) => {
    axios
      .get(`http://localhost:8000/get-receipt-stats/${receipt_number}`)
      .then((response) => {
        if (response.data.cart_items) {
          //console.log("Response", response.data.cart_items);
          cartItems = response.data.cart_items.map((item) => ({
            id: item.id,
            item_name: item.name,
            quantity: item.quantity,
          }));

          //console.log("initial cart items", receipt_data.cart_items);

          //console.log("new cart items:-", cartItems);
          setData(cartItems);
          // setMissingData(response.data.missing_items);
        } else if (response.data.cart_items) {
          console.error("No grouped_items available in the response");
        }
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };

  // checkObjectExists()

  const searchResultsWithDuplicates = hits.map((item) => {
  //  console.log("hits", item.name);
    //console.log("cart data:", cartItems);
    //console.log("cart data 2:", receipt_data.cart_items);
    //console.log("is in cart?", _.find(data, { item_name: item.name }));
    const exists = checkNameExists(data, item.name);

   // console.log("exisits", exists);

    return { ...item, exists };
  });

 // console.log("search results with dupes:", searchResultsWithDuplicates);

  const handleCardClick = (index) => {
    const result = Boolean(
      // _.find(data, {
      //   name: searchResultsWithDuplicates[index].name,
      // })

      checkNameExists(data, searchResultsWithDuplicates[index].name)
    );
    if (!result) {
      if (selectedCardIndex === index) {
        setSelectedCardIndex(-1);
        setQuantity(1);
      } else {
        setSelectedCardIndex(index);
        setQuantity(0);
      }
    }
  };

  const handleDecreaseClick = (event) => {
    event.stopPropagation(); // Stop event propagation
    if (quantity > 1) {
      const updatedQuantity = quantity - 1;
      setQuantity(updatedQuantity);
      console.log("Decreased quantity:", updatedQuantity);
    }
  };

  const handleInputChange = (value) => {
    const parsedValue = parseInt(value, 10);
    if (!isNaN(parsedValue) && parsedValue >= 1 && parsedValue <= 50) {
      const formattedValue = parsedValue.toString().padStart(2, "0");
      console.log("formated value:", typeof formattedValue);
      setQuantity(formattedValue);
      console.log("Input value changed:", formattedValue);
    }
  };

  const handleIncreaseClick = (event) => {
    event.stopPropagation(); // Stop event propagation
    const updatedQuantity = quantity + 1;
    if (updatedQuantity <= 50) {
      setQuantity(updatedQuantity);
      console.log("Increased quantity:", updatedQuantity);
    } else {
      console.log("Maximum quantity reached:", quantity);
    }
  };

  if (hasmore) {
    refinenext();
    get();
  }

  const handleSaveClick = () => {
    if (quantity === 0) {
      console.log("quantity is 0, enter quantity")
      alert("Quantity cannot be zero!")
      return; // Do not proceed if quantity is 0
    }


    console.log("Save clicked. Quantity:", quantity);
    const selectedItem = hits[selectedCardIndex];
    selectedItem.quantity = quantity;

    console.log("save receipt_number:", receipt_data.receipt_number);
    console.log("Save clicked. Item:", selectedItem);

    const url = `http://localhost:8000/add_cart_items_to_main_receipt/${receipt_data.receipt_number}`;

    axios
      .put(url, selectedItem)
      .then((response) => {
        if (
          response.data.message.startsWith(
            "Object already exists in cart_items for receipt"
          )
        ) {
          setResponseDuplicate(response.data.message);
        } else {
          console.log("PUT request successful:", response.data.message);
          alert("successfully saved in cart")
          fetchData(receipt_data.receipt_number);
        }
      })
      .catch((error) => {
        console.error("Error sending PUT request:", error);
      });

    get();

    setSelectedCardIndex(-1);
  };

  const handleCancelClick = () => {
    setSelectedCardIndex(-1);
    setQuantity(1);
  };

  return (
    <SimpleGrid
      spacing={14}
      templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
    >
      {responseDuplicate.startsWith("duplicate") && (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>{responseDuplicate}</AlertTitle>
        </Alert>
      )}

      {searchResultsWithDuplicates.map((item, index) => (
        <Card
          key={index}
          onClick={() => handleCardClick(index)}
          style={{ backgroundColor: item.exists ? "white" : "" }}
        >
          <CardHeader></CardHeader>
          {/* <CardBody>
            <Image
              src={item.item.image_url}
              alt="image not available"
              borderRadius="sm"
            />
            <Heading size="md"> name: {item.name}</Heading>
            <Text>{searchDu
              plicate}</Text>
          </CardBody> */}

          <CardBody className={item.exists ? "false" : "true"}>
            <Image
              src={item.item.image_url}
              alt="image not available"
              borderRadius="sm"
            />
            <Heading
              style={{ color: "black", fontSize: "16px", fontWeight: "bold" }}
              size="md"
            >
              {" "}
              name: {item.name}
            </Heading>
            {item.exists && (
              <Text
                className="already-in-cart-text"
                style={{ color: "black", fontSize: "16px", fontWeight: "bold" }}
              >
                ITEM ALREADY IN CART!!
              </Text>
            )}
          </CardBody>

          {selectedCardIndex === index && (
            <CardFooter>
              <HStack spacing={2}>
                <Button size="sm" onClick={handleDecreaseClick}>
                  -
                </Button>
                <input
                  type="number"
                  value={quantity}
                  onChange={handleInputChange}
                  min={0}
                  max={50}
                />
                <Button size="sm" onClick={handleIncreaseClick}>
                  +
                </Button>
              </HStack>
              <HStack mt={2} justify="flex-end">
                <Button colorScheme="teal" size="sm" disabled={quantity===0} onClick={handleSaveClick}>
                  Save
                </Button>
                <Button
                  colorScheme="teal"
                  size="sm"
                  onClick={handleCancelClick}
                >
                  Cancel
                </Button>
              </HStack>
            </CardFooter>
          )}
        </Card>
      ))}
    </SimpleGrid>
  );
};

const Results = connectInfiniteHits(SearchResultsComponent);

export default Results;
