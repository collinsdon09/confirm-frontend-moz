import React, { useEffect, useState } from "react";
// import TabsApp from "../components/Tabs_component";
import axios from "axios";
import { Text, Heading, Box, Stack, StackDivider } from "@chakra-ui/react"; // Import the Text component from the Chakra UI library

import { Routes, Route, useNavigate } from "react-router-dom";

import _ from "lodash";
import { has } from "lodash";
import getGroupedItems from "../functions/GetGroupedItems";
import { saveGroupedItems } from "../functions/SaveGroupedItems";
import { saveGroupedItemsRefund } from "../functions/SaveGroupedItemsRefund";

import { saveAutoConfirmedGroupedItems } from "../functions/SaveGroupedAutoConfirmed";
import { FaShoppingBasket } from "react-icons/fa";
import { WarningIcon } from "@chakra-ui/icons";
import ToggleColorMode from "./ToggleColormode";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
} from "@chakra-ui/react";

import {
  Card,
  SimpleGrid,
  Button,
  CardHeader,
  CardBody,
  CardFooter,
} from "@chakra-ui/react";
import { Tabs, TabList, TabPanels, Tab, TabPanel } from "@chakra-ui/react";

const WebSocketComponent = () => {
  const navigate = useNavigate();

  const [Opencards, setOpenCards] = useState([]);
  const [AutoConfirmedCards, setAutoConfirmedCards] = useState([]);
  const [Refundcards, setRefundCards] = useState([]);
  const [ConfirmedCards, setConfirmedCards] = useState([]);
  const [cartItemNumber, setCartItemsNumber] = useState(0);
  const [totalqtyLineItems, setLineItems] = useState(0);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [autoconfirmedCartItems, setautoconfirmedCartItems] = useState([]);
  const [refundCartItems, setrefundCartItems] = useState([]);
  const [confirmedCartItems, setConfirmedCartItems] = useState([]);
  const [confirmedMissingItems, setConfirmedMissingItems] = useState([]);

  const [isModal1Open, setIsModal1Open] = useState(false);
  const [isModal2Open, setIsModal2Open] = useState(false);

  var shouldUpdateIncomingReceipt = true;

  const fetchMessages = async () => {
    try {
      const response = await axios.get(
        // "http://localhost:8000/api/get-receipts-type",
        "http://localhost:8000/data-open-in-progress"
      );
      //setReceipts(response.data);
      //console.log("res:", response.data);
      const dataArray = Array.from(response.data);
      //console.log("all data", dataArray);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log("UNIQUE:-->", uniqueArray);

      // const sortedData  = dataArray.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

      const sortedData = uniqueArray.sort((a, b) => {
        const dateComparison = new Date(a.created_at) - new Date(b.created_at);
        if (dateComparison !== 0) {
          return dateComparison;
        }
        return b.age - a.age;
      });

      //console.log("sorted...", sortedData);

      sortedData.forEach(async (obj) => {
        //console.log("receipt number for the cart:","", item.receipt_number)
        //console.log("cart", obj.cart_items.length);
        //console.log(obj.receipt_number);

        //console.log("stats",obj.receipt_number)
        setOpenCards(uniqueArray);

        if (obj.status === "IN-PROGRESS") {
          //console.log("------STATUS IS IN-PROGRESS");

          const totalQtyCartItems = _.reduce(
            obj.cart_items,
            (accumulator, currentItem) => accumulator + currentItem.quantity,
            0
          );

          //console.log("total quantity for cart items", totalQtyCartItems)

          const inprogress_receipt = {
            receipt_number: obj.receipt_number,
            note: toString(obj.note),
            status: "IN-PROGRESS",
            receipt_type: obj.receipt_type,
            refund_for: toString(obj.note),
            order: toString(obj.order),
            created_at: obj.created_at,
            updated_at: obj.updated_at,
            source: obj.source,
            receipt_date: obj.receipt_date,
            cancelled_at: toString(obj["cancelled_at"]),
            total_money: obj["total_money"],
            total_tax: obj["total_tax"],
            points_earned: obj["points_earned"],
            points_deducted: obj["points_deducted"],
            points_balance: obj["points_balance"],
            customer_id: toString(obj["customer_id"]),
            total_discount: obj["total_discount"],
            employee_id: toString(obj["employee_id"]),
            store_id: obj["store_id"],
            pos_device_id: obj["pos_device_id"],
            dining_option: toString(obj["dining_option"]),
            total_discounts: obj["total_discounts"],
            total_taxes: obj["total_taxes"],
            tip: obj["tip"],
            surcharge: obj["surcharge"],
            line_items: obj["line_items"],
            cart_items: obj["cart_items"],
            missing_items: obj["missing_items"],
            line_items_total_qty: obj["line_items_total_qty"],
            cart_items_total_qty: totalQtyCartItems,
          };

           
        // obj.cart_items_total_qty = totalQtyCartItems

          

          

          //console.log( inprogress_receipt.receipt_number,"OPEN-TEST6", "upated in progress receipt:", inprogress_receipt)

          try {
            await axios.put(
              `http://localhost:8000/update-receipt/${inprogress_receipt.receipt_number}`,
              inprogress_receipt
            );
            //console.log(inprogress_receipt.receipt_number,"inprogress receipt  updated for total cart_qty", totalQtyCartItems);

            // Clear form inputs after successful update
            // setReceiptNumber('');
            // setStatus('');
          } catch (error) {
            console.error("Error updating receipt:", error);
          }

          setOpenCards(uniqueArray);
          shouldUpdateIncomingReceipt = false;
        } else {
          //console.log("*******STATUS  IS NOT IN-PROGRESS");
          if (obj.cart_items.length >= 1) {
            console.log("check status", shouldUpdateIncomingReceipt);
            // console.log(
            //   "cart items for",
            //   obj.receipt_number,
            //   "cart item",
            //   obj.cart_items.length
            // );

            const totalQtyCartItems = _.reduce(
              obj.cart_items,
              (accumulator, currentItem) => accumulator + currentItem.quantity,
              0
            );

            console.log("total quantity for cart items", totalQtyCartItems);

            // if(obj.cart_items_total_qty >0){

            // }else{

            // }

            const inprogress_receipt = {
              receipt_number: obj.receipt_number,
              note: toString(obj.note),
              status: "IN-PROGRESS",
              receipt_type: obj.receipt_type,
              refund_for: toString(obj.note),
              order: toString(obj.order),
              created_at: obj.created_at,
              updated_at: obj.updated_at,
              source: obj.source,
              receipt_date: obj.receipt_date,
              cancelled_at: toString(obj["cancelled_at"]),
              total_money: obj["total_money"],
              total_tax: obj["total_tax"],
              points_earned: obj["points_earned"],
              points_deducted: obj["points_deducted"],
              points_balance: obj["points_balance"],
              customer_id: toString(obj["customer_id"]),
              total_discount: obj["total_discount"],
              employee_id: toString(obj["employee_id"]),
              store_id: obj["store_id"],
              pos_device_id: obj["pos_device_id"],
              dining_option: toString(obj["dining_option"]),
              total_discounts: obj["total_discounts"],
              total_taxes: obj["total_taxes"],
              tip: obj["tip"],
              surcharge: obj["surcharge"],
              line_items: obj["line_items"],
              cart_items: obj["cart_items"],
              missing_items: obj["missing_items"],
              line_items_total_qty: obj["line_items_total_qty"],
              cart_items_total_qty: obj["cart_items_total_qty"],
            };

            const inprogress_receipt2 = {
              receipt_number: obj.receipt_number,
              note: toString(obj.note),
              status: "IN-PROGRESS",
              receipt_type: obj.receipt_type,
              refund_for: toString(obj.note),
              order: toString(obj.order),
              created_at: obj.created_at,
              updated_at: obj.updated_at,
              source: obj.source,
              receipt_date: obj.receipt_date,
              cancelled_at: toString(obj.cancelled_at),
              total_money: obj.total_money,
              total_tax: obj.total_money,
              points_earned: obj.points_earned,
              points_deducted: obj.points_deducted,
              points_balance: obj.points_balance,
              customer_id: toString(obj.customer_id),
              total_discount: obj["total_discount"],
              employee_id: toString(obj.total_discount),
              store_id: obj.store_id,
              pos_device_id: obj.pos_device_id,
              dining_option: toString(obj.dining_option),
              total_discounts: obj.total_discounts,
              total_taxes: obj.total_taxes,
              tip: obj.tip,
              surcharge: obj.surcharge,
              line_items: obj.line_items,
              cart_items: obj.cart_items,
              missing_items: obj.missing_items,
              line_items_total_qty: toString(obj.line_items_total_qty),
              cart_items_total_qty: toString(obj.cart_items_total_qty),
            };

            console.log("inprogress receipt detected", inprogress_receipt);

            try {
              await axios.put(
                `http://localhost:8000/update-receipt/${inprogress_receipt.receipt_number}`,
                inprogress_receipt
              );
              console.log("Receipt updated successfully");

              // Clear form inputs after successful update
              // setReceiptNumber('');
              // setStatus('');
            } catch (error) {
              console.error("Error updating receipt:", error);
            }
          } else {
          }
        }
      });
    } catch (error) {
      console.error(error);
    }
  };

  const fetAutoConfirmed = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-auto-confirmed-receipts",
        {
          params: { status: "AUTO-CONFIRMED" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

    

      setAutoConfirmedCards(dataArray);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const handleModal1Close = () => {
    setIsModal1Open(false);
  };

  const fetchRefund = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-receipts-refund",
        {
          params: { status: "REFUND" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      // console.log(" auto-confirmed:", uniqueArray);

      // const sortedData = uniqueArray.sort((a, b) => {
      //   const dateComparison = new Date(a.created_at) - new Date(b.created_at);
      //   if (dateComparison !== 0) {
      //     return dateComparison;
      //   }
      //   return b.age - a.age;
      // });

      const sortedData = uniqueArray.sort((a, b) => {
        const dateComparison = new Date(b.created_at) - new Date(a.created_at);
        if (dateComparison !== 0) {
          return dateComparison;
        }
        return a.age - b.age;
      });

      // console.log("sorted...", sortedData);

      setRefundCards(sortedData);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const storeMessages = async (modifiedObject) => {
    console.log("reciept number", modifiedObject.receipt_number);

    const sales_receipt = {
      receipt_number: modifiedObject.receipt_number,
      note: toString(modifiedObject.note),
      status: "OPEN",
      receipt_type: modifiedObject.receipt_type,
      refund_for: toString(modifiedObject.note),
      order: toString(modifiedObject.order),
      created_at: modifiedObject.created_at,
      updated_at: modifiedObject.updated_at,
      source: modifiedObject.source,
      receipt_date: modifiedObject.receipt_date,
      cancelled_at: toString(modifiedObject["cancelled_at"]),
      total_money: modifiedObject["total_money"],
      total_tax: modifiedObject["total_tax"],
      points_earned: modifiedObject["points_earned"],
      points_deducted: modifiedObject["points_deducted"],
      points_balance: modifiedObject["points_balance"],
      customer_id: toString(modifiedObject["customer_id"]),
      total_discount: modifiedObject["total_discount"],
      employee_id: toString(modifiedObject["employee_id"]),
      store_id: modifiedObject["store_id"],
      pos_device_id: modifiedObject["pos_device_id"],
      dining_option: toString(modifiedObject["dining_option"]),
      total_discounts: modifiedObject["total_discounts"],
      total_taxes: modifiedObject["total_taxes"],
      tip: modifiedObject["tip"],
      surcharge: modifiedObject["surcharge"],
      line_items: modifiedObject["line_items"],
      cart_items: [],

      line_items_total_qty: modifiedObject["line_items_total_qty"],
      cart_items_total_qty: modifiedObject["cart_items_total_qty"],
      missing_items: ['missing_items']
    };

    const sales_receipt2 = {
      receipt_number: modifiedObject.receipt_number,
      note: toString(modifiedObject.note),
      status: "OPEN",
      receipt_type: modifiedObject.receipt_type,
      refund_for: toString(modifiedObject.note),
      order: toString(modifiedObject.order),
      created_at: modifiedObject.created_at,
      updated_at: modifiedObject.updated_at,
      source: modifiedObject.source,
      receipt_date: "string",
      cancelled_at: "string",
      total_money: 0,
      total_tax: 0,
      points_earned: 0,
      points_deducted: 0,
      points_balance: 0,
      customer_id: "string",
      total_discount: 0,
      employee_id: "string",
      store_id: "string",
      pos_device_id: "string",
      dining_option: "string",
      total_discounts: [],
      total_taxes: ["string"],
      tip: 0,
      surcharge: 0,
      line_items: ["string"],
      cart_items: ["string"],
      line_items_total_qty: 0,
      cart_items_total_qty: 0,
      // "total_qty": "string"
      missing_items: []
    };

    console.log("receipt to be saved", sales_receipt);
    try {
      await axios.post(
        "http://localhost:8000/save_receipt_cart_items_and_confirmed_items",
        sales_receipt
      );
    } catch (error) {
      console.error("Error storing messages:", error);
    }
  };

  const storeMessagesAutoConfirmedMessages = async (modifiedObject) => {
    console.log("storing autoconfirmed receipts...");
    console.log("reciept number", modifiedObject);

    const url = "http://localhost:8000/save_receipt-autoconfirmed";
  
    try {
      const response = axios.post(url, modifiedObject);
      //console.log(response.data); // Handle the response data as needed
    } catch (error) {
      console.error(error); // Handle any errors that occurred during the request
    }
  };

  const storeRefundCards = async (ref_receipt) => {
    axios
      .post(
        "http://localhost:8000/save_receipt-refund",

        ref_receipt
      )
      .then((response) => {
        console.log("POST SUCCESSFUL FOR REFUND RECEIPT");
        // Handle success
        //console.log(response.data);
      })
      .catch((error) => {
        // Handle error
        console.error(error);
      });
  };

  const handleAutoConfirmedClick = (cardData) => {
    navigate("/auto-confirmed-view", { state: cardData });
    // onOpen();

    // const namesArray = [];
    // //setLineItems(receipt_data.line_items);
    // const url = `http://localhost:8000/get-cart_items_in_auto_confirmed/${cardData.receipt_number}`;
    // axios
    //   .get(url)
    //   .then((response) => {
    //     console.log("Autoconfirmed items", response.data);
    //     setautoconfirmedCartItems(response.data.grouped_items);
    //   })
    //   .catch((error) => {
    //     console.error("Error:", error);
    //   });
  };

  const handleRefundClick = (cardData) => {
    navigate("/refund-view", { state: cardData });
  };

  const fetchConfirmedReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-confirmed-receipts",
        {
          params: { status: "CONFIRMED" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log(" confirmed:", uniqueArray);

      // const filteredCards = uniqueArray.filter(
      //   (item) => item.total_money > 100
      // );
      //console.log("filtered",filteredCards);
      setConfirmedCards(uniqueArray);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const handleConfirmedClick = (cardData) => {
    navigate("/confirmed-view", { state: cardData });
    // setIsModal2Open(true);

    // const namesArray = [];
    // //setLineItems(receipt_data.line_items);
    // console.log("card data:", cardData)
    // const url = `http://localhost:8000/get-cart_items_in_confirmed/${cardData.receipt_number}`;
    // axios
    //   .get(url)
    //   .then((response) => {
    //     console.log("confirmed items", response.data.cart_items);
    //     setConfirmedCartItems(response.data.cart_items);
    //     setConfirmedMissingItems(response.data.missing_items);
    //     console.log("missing items:->", response.data.missing_items);
    //   })
    //   .catch((error) => {
    //     console.error("Error:", error);
    //   });

    //console.log("updatedcard",response.data.line_items)

    //setConfirmedCartItems(cardData.cart_items)
  };

  const handleModal2Close = () => {
    setIsModal2Open(false);
  };

  const handleCardClick = (cardData, func) => {
    navigate("/receipt", { state: cardData });
  };

  useEffect(() => {
    // fetchMessages();
    // fetAutoConfirmed();
    // fetchRefund();
    // fetchConfirmedReceipts();

    // Function to handle WebSocket connection
    const setupWebSocket = () => {
      //Create a WebSocket connection
      //const socket = new WebSocket("ws://localhost:8080");

       const userId = "some-random-id";
       const socket = new WebSocket(
          `wss://n4w9byf0tj.execute-api.eu-central-1.amazonaws.com/prod?userId=${userId}`
        );

      // Connection opened
      socket.addEventListener("open", (event) => {
        console.log("WebSocket connection opened");

        // Send a message after the connection is open
        socket.send("hi");
      });

      // Connection closed
      socket.addEventListener("close", (event) => {
        console.log("WebSocket connection closed");
      });

      // Error occurred
      socket.addEventListener("error", (event) => {
        console.error("WebSocket error:", event);
      });

      // Message received
      socket.addEventListener("message", (event) => {
        //console.log("WebSocket message:", event.data);
        const receivedData = JSON.parse(event.data);

        receivedData.receipts.forEach((receipt, index) => {
          const receiptType = receipt.receipt_type;
          console.log(`Receipt ${index + 1} Type:`, receiptType);

          // const totalQty = receipt["line_items"].reduce(
          //   (accumulator, currentItem) => accumulator + currentItem.quantity,
          //   0
          // );

          const totalQtyLineItems = _.reduce(
            receipt.line_items,
            (accumulator, currentItem) => accumulator + currentItem.quantity,
            0
          );

          const totalQtyCartItems = _.reduce(
            receipt.cart_items,
            (accumulator, currentItem) => accumulator + currentItem.quantity,
            0
          );

          console.log("TOTAL QTY", totalQtyLineItems);

          receipt.line_items_total_qty = totalQtyLineItems;
          receipt.cart_items_total_qty = totalQtyCartItems;
          console.log("receipt after adding total qtys:", receipt);
          const receipt2 = receipt;

          // receipt = receipt.line_items_total_qty

          if (receiptType === "SALE") {
            //console.log("receipt", receipt);
            console.log("SALE RECEIPT DETECTED");
            //receivedData.status = "SALE";
            const modifiedObject = receipt2;
            console.log("whole receipt:", receipt2);
            //console.log("whole receipt2", receivedData)

            console.log("MODIFIED:", modifiedObject["line_items"]);

            const totalQty = _.reduce(
              receipt.cart_items,
              (accumulator, currentItem) => accumulator + currentItem.quantity,
              0
            );

            console.log("Grand Total:-", modifiedObject["total_money"]);

            if (modifiedObject["total_money"] > 1 || totalQty > 1) {
              modifiedObject.status = "OPEN";

              console.log(
                "receipt met the Grand Total Requirements",
                modifiedObject["receipt_number"]
              );

              const hasDuplicates = !_.isEqual(
                _.uniq(modifiedObject["line_items"], "item_id"),
                modifiedObject["line_items"]
              );

              console.log("HAS DUPLICATES ITEMS??-->", hasDuplicates);

              if (hasDuplicates) {
                //if it has duplicate itemIds
                console.log("** has duplicate item ids so we group them");
                console.log(
                  "grouped items:-",
                  getGroupedItems(modifiedObject["line_items"])
                );
                console.log(
                  "confirmed receipt number:",
                  modifiedObject["receipt_number"]
                );

                saveGroupedItems(
                  getGroupedItems(modifiedObject["line_items"]),
                  modifiedObject
                );
              }
              storeMessages(modifiedObject); //save them first

              setOpenCards((prevMessages) => {
                const uniqueMessages = [...prevMessages];
                if (
                  !uniqueMessages.find(
                    (message) =>
                      message.receipt_number === modifiedObject.receipt_number
                  )
                ) {
                  uniqueMessages.push(modifiedObject);
                }
                return uniqueMessages;
              });

              //storeMessages(modifiedObject);
            } else {
              // AUTO CONFIRMED
              const hasDuplicates = !_.isEqual(
                _.uniq(modifiedObject["line_items"], "item_id"),
                modifiedObject["line_items"]
              );

              console.log(" AUTO HAS DUPLICATES ITEMS??-->", hasDuplicates);

              if (hasDuplicates) {
                //console.log("auto-confirmed has duplicates");

                saveAutoConfirmedGroupedItems(
                  getGroupedItems(modifiedObject["line_items"]),
                  modifiedObject
                );
              }

              console.log(
                "receipt did not meet the Grand Total Requirements, AUTO CONFIRMED RECEIPT DETECTED",
                receipt
              );
              //POST STATUS to AUTO-CONFIRMED
              //modifiedObject.status = "AUTO-CONFIRMED";
              console.log("AUTO CONFIRMED RECEIPT");

              console.log(
                "receipt met the Grand Total Requirements",
                modifiedObject["receipt_number"]
              );
              //const url = "http://localhost:8000/save_receipt_cart_items_and_confirmed_items"; // Replace with your API endpoint URL

              

              const totalQty = _.reduce(
                receipt2["line_items"],
                (accumulator, currentItem) =>
                  accumulator + currentItem.quantity,
                0
              );

             // console.log("total qty auto-confirmed:", totalQty);


              const auto_confirmed_object = {
                receipt_number: modifiedObject.receipt_number,
                note: toString(modifiedObject.note),
                status: "REFUND",
                receipt_type: modifiedObject.receipt_type,
                refund_for: toString(modifiedObject.note),
                order: toString(modifiedObject.order),
                created_at: modifiedObject.created_at,
                updated_at: modifiedObject.updated_at,
                source: modifiedObject.source,
                receipt_date: modifiedObject.receipt_date,
                cancelled_at: toString(modifiedObject["cancelled_at"]),
                total_money: modifiedObject["total_money"],
                total_tax: modifiedObject["total_tax"],
                points_earned: modifiedObject["points_earned"],
                points_deducted: modifiedObject["points_deducted"],
                points_balance: modifiedObject["points_balance"],
                customer_id: toString(modifiedObject["customer_id"]),
                total_discount: modifiedObject["total_discount"],
                employee_id: toString(modifiedObject["employee_id"]),
                store_id: modifiedObject["store_id"],
                pos_device_id: modifiedObject["pos_device_id"],
                dining_option: toString(modifiedObject["dining_option"]),
                total_discounts: modifiedObject["total_discounts"],
                total_taxes: modifiedObject["total_taxes"],
                tip: modifiedObject["tip"],
                surcharge: modifiedObject["surcharge"],
                line_items: modifiedObject["line_items"],
                cart_items: [],
                line_items_total_qty: modifiedObject["line_items_total_qty"],
              };

              storeMessagesAutoConfirmedMessages(auto_confirmed_object)


              setAutoConfirmedCards((prevMessages) => {
                const uniqueMessages = [...prevMessages];
                if (
                  !uniqueMessages.find(
                    (message) =>
                      message.receipt_number === modifiedObject.receipt_number
                  )
                ) {
                  uniqueMessages.push(modifiedObject);
                }
                return uniqueMessages;
              });
















              //storeMessages(modifiedObject);

              //storeMessagesAutoConfirmedMessages(modifiedObject);
            }
          } else if (receiptType === "REFUND") {
            //REFUND

            console.log("REFUND receipt", receipt2);
            console.log("REFUND RECEIPT DETECTED");
            // receivedData.status = "REFUND";
            const hasDuplicates = !_.isEqual(
              _.uniq(receipt2["line_items"], "item_id"),
              receipt2["line_items"]
            );

            console.log("HAS DUPLICATES ITEMS??-->", hasDuplicates);

            if (hasDuplicates) {
              console.log("refund has duplicates");

              saveGroupedItemsRefund(
                getGroupedItems(receipt2["line_items"]),
                receipt2
              );
            }

            const totalQty = _.reduce(
              receipt2["line_items"],
              (accumulator, currentItem) => accumulator + currentItem.quantity,
              0
            );

            console.log("total qty refund:", totalQty);

            const modifiedObject = receipt2;

            const refund_receipt = {
              receipt_number: modifiedObject.receipt_number,
              note: toString(modifiedObject.note),
              status: "REFUND",
              receipt_type: modifiedObject.receipt_type,
              refund_for: toString(modifiedObject.note),
              order: toString(modifiedObject.order),
              created_at: modifiedObject.created_at,
              updated_at: modifiedObject.updated_at,
              source: modifiedObject.source,
              receipt_date: modifiedObject.receipt_date,
              cancelled_at: toString(modifiedObject["cancelled_at"]),
              total_money: modifiedObject["total_money"],
              total_tax: modifiedObject["total_tax"],
              points_earned: modifiedObject["points_earned"],
              points_deducted: modifiedObject["points_deducted"],
              points_balance: modifiedObject["points_balance"],
              customer_id: toString(modifiedObject["customer_id"]),
              total_discount: modifiedObject["total_discount"],
              employee_id: toString(modifiedObject["employee_id"]),
              store_id: modifiedObject["store_id"],
              pos_device_id: modifiedObject["pos_device_id"],
              dining_option: toString(modifiedObject["dining_option"]),
              total_discounts: modifiedObject["total_discounts"],
              total_taxes: modifiedObject["total_taxes"],
              tip: modifiedObject["tip"],
              surcharge: modifiedObject["surcharge"],
              line_items: modifiedObject["line_items"],
              cart_items: [],
              line_items_total_qty: modifiedObject["line_items_total_qty"],
            };

            console.log("new receipt:", refund_receipt);

            setRefundCards((prevMessages) => {
              const uniqueMessages = [...prevMessages];
              if (
                !uniqueMessages.find(
                  (message) =>
                    message.receipt_number === refund_receipt.receipt_number
                )
              ) {
                uniqueMessages.push(refund_receipt);
              }
              return uniqueMessages;
            });

            //storeMessages(modifiedObject);

            storeRefundCards(refund_receipt);
          }
        });
      });

      // Clean up the WebSocket connection on component unmount
      return () => {
        socket.close();
      };
    };

    // Call the function to set up the WebSocket connection
    setupWebSocket();
      fetchMessages();
    fetAutoConfirmed();
    fetchRefund();
    fetchConfirmedReceipts();
  }, []);

  return (
    <>
      <h1>Websockets</h1>
      <ToggleColorMode />
      <Tabs variant="enclosed">
        <TabList>
          {/* <Tab>To Be confirmed</Tab>
          <Tab>Auto-Confirmed</Tab>
          <Tab>Refund</Tab>
          <Tab>Confirmed</Tab>
           */}

          <Tab fontWeight="extrabold">To Be confirmed</Tab>
          <Tab fontWeight="extrabold">Auto-Confirmed</Tab>
          <Tab fontWeight="extrabold">Refund</Tab>
          <Tab fontWeight="extrabold">Confirmed</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            >
              {Opencards.map((card, index) => {
                if (card.status === "OPEN" || card.status === "IN-PROGRESS") {
                  return (
                    <Card
                      className="card"
                      width="150px"
                      height="200px"
                      padding="5px"
                      key={index}
                      onClick={() => handleCardClick(card)}
                    >
                      {/* Card content */}
                      <CardHeader>
                        {/* <Heading size="lg" className="Heading">
                          {card.receipt_number}
                          {card.created_at.slice(11, 19)}
                   
                        </Heading> */}

                        <Heading size="md" className="Heading">
                          <div
                            style={{
                              display: "flex",
                              alignItems: "left",
                            }}
                          >
                            <span
                              style={{
                                marginRight: "6px", // Adjust the spacing between the elements
                              }}
                            >
                              {card.receipt_number}
                            </span>
                            <span>{card.created_at.slice(11, 19)}</span>
                          </div>
                        </Heading>
                      </CardHeader>
                      <CardBody>
                        {/* <Text>{card.customer_id}</Text> */}
                        {/* <Text>STATUS: {card.status}</Text> */}
                      </CardBody>
                      <div className="status-infor">
                        <FaShoppingBasket color="	#0000ff" />
                        <Text>
                          {/* {card.cart_items.length}/{card.line_items.length} */}
                          {card.cart_items_total_qty}/
                          {card.line_items_total_qty}
                        </Text>
                      </div>

                      <CardFooter>
                        {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                        <div className="card-header">
                          <Button
                            className="btn"
                            //color="#FF0000"
                            color={
                              card.status === "IN-PROGRESS" ? "green" : "red"
                            }
                            bgColor="transparent"
                            fontWeight="800"
                            fontSize="xl"
                          >
                            <WarningIcon color="#FF0000" />
                            {card.status}
                          </Button>
                        </div>
                      </CardFooter>
                    </Card>
                  );
                }
                return null;
              })}
            </SimpleGrid>
          </TabPanel>

          <TabPanel>
            <p>Auto-confirmed Tab</p>
            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
              // Add a unique key prop
            >
              {AutoConfirmedCards.map((card, index) => {
                // console.log("index", index,":", card)

                // if (card.status === "REFUND") {
                //console.log("AUTO CONFIRMED STATTUS", card.status);
                return (
                  <>
                    <Card
                      className="card"
                      width="full"
                      height="200px"
                      key={card.receipt_number}
                      onClick={() => handleAutoConfirmedClick(card)}
                    >
                      {/* Card content */}
                      <CardHeader>
                        {/* <Stats receipt_props={card.receipt_number} /> */}
                        <Heading size="sm" className="Heading">
                          <div
                            style={{
                              display: "flex",
                              alignItems: "left",
                            }}
                          >
                            <span
                              style={{
                                marginRight: "6px", // Adjust the spacing between the elements
                              }}
                            >
                              {card.receipt_number}
                            </span>
                            <span>{card.created_at.slice(11, 19)}</span>
                          </div>
                        </Heading>
                      </CardHeader>
                      <CardBody>
                        <Text>card.customer_id</Text>
                        {/* <Text>STATUS: {card.status}</Text> */}
                      </CardBody>
                      <div className="status-infor">
                        <FaShoppingBasket color="	#0000ff" />
                        <Text>{card.line_items_total_qty}</Text>
                      </div>

                      <CardFooter>
                        {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                        <div className="card-header">
                          <Button
                            className="btn"
                            color="#FF0000"
                            bgColor="transparent"
                            fontWeight="800"
                            fontSize="xl"
                          >
                            <WarningIcon color="#FF0000" />
                            {card.status}
                          </Button>
                        </div>
                      </CardFooter>
                    </Card>

                    {/* AUTO CONFIRMED MODAL */}
                    <Modal isOpen={isOpen} onClose={onClose}>
                      <ModalOverlay />
                      <ModalContent>
                        <ModalHeader>
                          AutoConfirmed Line Items for: {card.receipt_number}
                        </ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                          {/* <CompareArrays
                            array1={lineItems}
                            array2={cartItems}
                            groupedItems={groupedItems}
                            receipt_data_prop={receipt_data}
                          /> */}

                          <ul style={{ display: "flex", flexWrap: "wrap" }}>
                            {autoconfirmedCartItems.map((item) => (
                              <li
                                style={{
                                  border: `3px solid green`,
                                  borderRadius: "10px",
                                  padding: "10px",
                                  marginBottom: "10px",
                                }}
                                key={item.id}
                              >
                                <h1 style={{ marginRight: "10px" }}>
                                  Item name{item.item_name}
                                </h1>
                                <p> Qty: {item.quantity}</p>
                              </li>
                            ))}
                          </ul>
                        </ModalBody>
                      </ModalContent>
                    </Modal>
                  </>
                );
              })}
            </SimpleGrid>
          </TabPanel>
          <TabPanel>
            <p>Refund</p>

            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            >
              {Refundcards.map((card, index) => {
                if (card.status === "REFUND") {
                  return (
                    <>
                      <Card
                        className="card"
                        width="full"
                        height="200px"
                        key={index}
                        onClick={() => handleRefundClick(card)}
                      >
                        {/* Card content */}
                        <CardHeader>
                          {/* <Stats receipt_props={card.receipt_number} /> */}
                          <Heading size="sm" className="Heading">
                            <div
                              style={{
                                display: "flex",
                                alignItems: "left",
                              }}
                            >
                              <span
                                style={{
                                  marginRight: "6px", // Adjust the spacing between the elements
                                }}
                              >
                                {card.receipt_number}
                              </span>{" "}
                              <span>{card.created_at.slice(11, 19)}</span>
                            </div>
                          </Heading>
                        </CardHeader>
                        <CardBody>
                          <Text>card.customer_id</Text>
                          {/* <Text>STATUS: {card.status}</Text> */}
                        </CardBody>
                        <div className="status-infor">
                          <FaShoppingBasket color="	#0000ff" />
                          <Text>{card.line_items_total_qty}</Text>
                        </div>

                        <CardFooter>
                          {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                          <div className="card-header">
                            <Button
                              className="btn"
                              color="#FF0000"
                              bgColor="transparent"
                              fontWeight="800"
                              fontSize="xl"
                            >
                              <WarningIcon color="#FF0000" />
                              {card.status}
                            </Button>
                          </div>
                        </CardFooter>
                      </Card>

                      {/* REFUND MODAL */}
                      <Modal isOpen={isModal1Open} onClose={handleModal1Close}>
                        <ModalOverlay />
                        <ModalContent>
                          <ModalHeader>
                            {/* Refund Items for {card.receipt_number} */}
                            Refund Items
                          </ModalHeader>
                          <ModalCloseButton />
                          <ModalBody>
                            {/* <CompareArrays
                            array1={lineItems}
                            array2={cartItems}
                            groupedItems={groupedItems}
                            receipt_data_prop={receipt_data}
                          /> */}

                            <ul style={{ display: "flex", flexWrap: "wrap" }}>
                              {refundCartItems.map((item) => (
                                <li
                                  style={{
                                    border: `3px solid green`,
                                    borderRadius: "10px",
                                    padding: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={item.id}
                                >
                                  <h1 style={{ marginRight: "10px" }}>
                                    Item name{item.item_name}
                                  </h1>
                                  <p> Qty: {item.quantity}</p>
                                </li>
                              ))}
                            </ul>
                          </ModalBody>
                        </ModalContent>
                      </Modal>
                    </>
                  );
                }
                return null;
              })}
            </SimpleGrid>
          </TabPanel>

          <TabPanel>
            confirmed
            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            >
              {ConfirmedCards.map((card_confirmed, index_confirmed) => {
                if (card_confirmed.status === "CONFIRMED") {
                  return (
                    <>
                      <Card
                        className="card"
                        width="full"
                        height="200px"
                        key={index_confirmed}
                        onClick={() => handleConfirmedClick(card_confirmed)}
                      >
                        {/* Card content */}
                        <CardHeader>
                          {/* <Stats receipt_props={card.receipt_number} /> */}
                          <Heading size="sm" className="Heading">
                            <div
                              style={{
                                display: "flex",
                                alignItems: "left",
                              }}
                            >
                              <span
                                style={{
                                  marginRight: "6px", // Adjust the spacing between the elements
                                }}
                              >
                                {card_confirmed.receipt_number}
                              </span>
                              <span>
                                {card_confirmed.created_at.slice(11, 19)}
                              </span>
                            </div>
                          </Heading>
                        </CardHeader>
                        <CardBody>
                          <Text>{card_confirmed.customer_id}</Text>
                          {/* <Text>STATUS: {card.status}</Text> */}
                        </CardBody>
                        <div className="status-infor">
                          <FaShoppingBasket color="	#0000ff" />
                          <Text>{card_confirmed.line_items_total_qty}</Text>
                        </div>

                        <CardFooter>
                          {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                          <div className="card-header">
                            <Button
                              className="btn"
                              color="#FF0000"
                              bgColor="transparent"
                              fontWeight="800"
                              fontSize="xl"
                            >
                              <WarningIcon color="#FF0000" />
                              {card_confirmed.status}
                            </Button>
                          </div>
                        </CardFooter>
                      </Card>

                      {/* CONFIRMED MODAL */}
                      {/* REFUND MODAL */}
                      <Modal isOpen={isModal2Open} onClose={handleModal2Close}>
                        <ModalOverlay />
                        <ModalContent>
                          <ModalHeader>
                            Confirmed Items for {card_confirmed._id}
                          </ModalHeader>
                          <ModalCloseButton />
                          <ModalBody>
                            {/* <CompareArrays
                            array1={lineItems}
                            array2={cartItems}
                            groupedItems={groupedItems}
                            receipt_data_prop={receipt_data}
                          /> */}
                            <p>Cart Items</p>
                            <ul style={{ display: "flex", flexWrap: "wrap" }}>
                              {confirmedCartItems.map((item) => (
                                <li
                                  style={{
                                    border: `3px solid green`,
                                    borderRadius: "10px",
                                    padding: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={item.id}
                                >
                                  <h1 style={{ marginRight: "10px" }}>
                                    Item name{item.item_name}
                                  </h1>
                                  <p> Qty: {item.quantity}</p>
                                </li>
                              ))}
                            </ul>

                            <p>MISSING ITEMS:-</p>
                            <ul style={{ display: "flex", flexWrap: "wrap" }}>
                              {confirmedMissingItems.map((item) => (
                                <li
                                  style={{
                                    border: `3px solid red`,
                                    borderRadius: "10px",
                                    padding: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={item.id}
                                >
                                  <h1 style={{ marginRight: "10px" }}>
                                    Item name{item.item_name}
                                  </h1>
                                  <p> Qty: {item.quantity}</p>
                                </li>
                              ))}
                            </ul>
                          </ModalBody>
                        </ModalContent>
                      </Modal>
                    </>
                  );
                }
                return null;
              })}
            </SimpleGrid>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  );
};

export default WebSocketComponent;
