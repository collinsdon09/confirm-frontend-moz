

import React, { useState, useEffect } from "react";
import _ from "lodash";
import { Button } from "@chakra-ui/react";

export default function CartItemValidity({
  receipt_props,
  lineItems,
  groupedItems,
  cartItems,
}) {
  const [isAdminButtonDisabled, setIsAdminButtonDisabled] = useState(true);
  const [isConfirmButtonDisabled, setIsConfirmButtonDisabled] = useState(false);
  const [matchingItems, setMatchingItems] = useState([]);

  useEffect(() => {
    let correctItemCount = 0;
    const matchingItems = cartItems.map((cartItem) => {
      if (receipt_props.grouped_items.length === 0) {
        const lineItem = _.find(lineItems, { item_name: cartItem.name });

        if (lineItem) {
          let output = `Correct item name: ${cartItem.name}`;

          if (cartItem.quantity === lineItem.quantity) {
            output += ` Correct qty ${cartItem.quantity}`;
            correctItemCount++;
            return { name: cartItem.name, quantity: cartItem.quantity };
          } else {
            output += ` Incorrect qty ${cartItem.quantity}`;
            setIsConfirmButtonDisabled(true);
            return null;
          }
        } else {
          setIsConfirmButtonDisabled(true);
          return null;
        }
      } else if (receipt_props.grouped_items.length > 0) {
        const groupItems = _.filter(receipt_props.grouped_items, {
          item_name: cartItem.name,
        });

        if (groupItems.length > 0) {
          let output = `Correct item name: ${cartItem.name}`;

          const matchingItem = _.find(groupItems, {
            quantity: cartItem.quantity,
          });

          if (matchingItem) {
            output += ` Correct qty ${cartItem.quantity}`;
            correctItemCount++;
            return { name: cartItem.name, quantity: cartItem.quantity };
          } else {
            output += ` Incorrect qty ${cartItem.quantity}`;
            return null;
          }
        } else {
          return null;
        }
      }
    });

    setIsAdminButtonDisabled(correctItemCount === 0);
    setIsConfirmButtonDisabled(correctItemCount === 0);
    setMatchingItems(matchingItems.filter((item) => item !== null));
  }, [cartItems, lineItems, receipt_props.grouped_items]);

  return (
    <div>
      <h2>Results:</h2>
      <ul>
        {cartItems.map((cartItem, index) => (
          <li
            key={index}
            style={{
              background: matchingItems.some(
                (item) =>
                  item.name === cartItem.name && item.quantity === cartItem.quantity
              )
                ? "green"
                : "red",
              color: "black",
              padding: "5px",
              margin: "5px",
            }}
          >
            {`Item name: ${cartItem.name}, Quantity: ${cartItem.quantity}`}
          </li>
        ))}
      </ul>
      <p>Correct Item Count: {matchingItems.length}</p>
      <Button isDisabled={isAdminButtonDisabled}>Admin</Button>
      <Button isDisabled={isConfirmButtonDisabled}>Confirm</Button>
    </div>
  );
}
