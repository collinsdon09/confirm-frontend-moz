import React from "react";
import { Button } from "@chakra-ui/react";
import axios from "axios";
import { useState, useEffect } from "react";
import _ from "lodash";

//import SaveMissingItems from "./SaveMissing";
// import { saveMissingItems } from './SaveMissingItems';

// import { compare_lodash_func } from "./Comparison_Lodash";

// import { saveMissingItems } from "./SaveMissing";
//import { checkCartItemValidity } from "../functions/Comparison_Lodash";
import { saveMissingItems } from "../functions/SaveMissing";
// const missing_itemsarr = [];

function CompareArrays({ array1, array2, groupedItems, receipt_data_prop }) {
  const [totalQuantity, setTotalQuantity] = useState(0);
  const [data, setData] = useState(null);
  const [isGroupedd, setIsGrouped] = useState(false);
  const [receiptData, setReceiptData] = useState("");
  //const [quantity, setQuantity] = useState("");

  //const [isAdminButtonDisabled, setIsAdminButtonDisabled] = useState(true);
  var isAdminButtonDisabled = false;
  var isGrouped = false;

  console.log("g items---->", groupedItems);
  //const [missingItemsNames, setMissingItemsNames] = useState([]);

  // console.log("receipt:", receipt_data_prop.grouped_items.length);

  if (groupedItems.length > 0) {
    console.log("*GROUPED ITEMS RECEIPT TAGGED");
    isGrouped = true;
  }

  useEffect(() => {
    axios
      .get(
        `http://localhost:8000/get-cart_items_in_receipt/${receipt_data_prop.receipt_number}`
      )
      .then((response) => {
        console.log("receipt data fetched>>>>>>>:", response.data);
        setData(response.data.grouped_items);
        //setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        //setLoading(false);
      });
  }, [receipt_data_prop.receipt_number]);

  //console.log("receiving g items", receipt_data_prop.grouped_items);

  if (groupedItems.length > 0) {
    console.log(">>>>>>>>>>>>>>>condition is met");
    console.log(">>>>>>>>>>>>>>>condition is met");
    array1.splice(0, array1.length, ...groupedItems);
    console.log("new g items", array1);
  } else {
    console.log(">>>>>>>>>condition is not met");
    console.log("array items are", array1);
  }

  var missingItemsNames = [];
  const matchingItemsName = [];
  var missingGroupItems = [];

  // function getRemainingItems(line_items, cart_items) {
  //   return line_items.filter((line_item) => {
  //     return !cart_items.some(
  //       (cart_item) => cart_item.name === line_item.item_name
  //     );
  //   });
  // }

  const handleDeleteItem = async (receipt_number, item_name) => {
    try {
      const response = await axios.delete(
        `http://localhost:8000/delete_cart_item/${receipt_number}/items/${item_name}`
      );
      console.log(response.data.message); // Optional: Log the response message
      // Perform any additional actions after successful deletion
      alert(response);
    } catch (error) {
      console.error(error);
      // Handle error scenario
    }
  };

  const updateItemQuantity = async (receiptNumber, itemName) => {
    // Prepare the payload
    // const payload = {
    //   quantity: quantity,
    // };
    const quantity = '6'
    toString(itemName)

    // Make the API call to update the item quantity
    const url = `http://localhost:8000/edit_cart_item/${receiptNumber}/items/${toString(itemName)
  }/quantity?quantity=7`;
    //http://localhost:8000/edit_cart_item/10-48417/items/Chamdor%20Champagne/quantity?quantity=6

    const url2 = `http://localhost:8000/edit_cart_item/10-48417/items/Chamdor%20Champagne/quantity?quantity=6`

    

    // Make the API call to update the item quantity
    fetch(url2, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
     // body: JSON.stringify({ quantity }),
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle the response data as needed
        console.log("Item quantity updated successfully:", data);
      })
      .catch((error) => {
        // Handle any errors that occurred during the API call
        console.error("Error updating item quantity:", error);
      });

    // console.log("receiving", receiptNumber, itemName);
    // const quantity = 3

    // const endpoint = `http://localhost:8000/edit_cart_item/${receiptNumber}/items/${itemName}/quantity${quantity}`;

    // return axios.put(endpoint)
    //   .then(response => {
    //     // Handle the response data
    //     console.log(response.data);
    //     // Return the response data or perform other operations
    //     return response.data;
    //   })
    //   .catch(error => {
    //     // Handle any errors
    //     console.error(error);
    //     // Return an error message or perform other error handling
    //     throw error;
    //   });

    // // const userInput = window.prompt("Enter the new quantity:");
    // // const newQuantity = parseInt(userInput, 10);
    // console.log("quantity entered", newQuantity);
    // if (!Number.isNaN(newQuantity)) {
    //   const obj = {
    //     quantity: toString(newQuantity),
    //   };
    //   console.log("type of newQuantity", typeof newQuantity)
    //    let quantity = 3

    //   const url = `http://localhost:8000/edit_cart_item/${receiptNumber}/items/${itemName}/quantity`;

    //   try {
    //     await axios.put(url, {quantity});
    //     console.log("PUT request successful");
    //   } catch (error) {
    //     console.error("Error:", error);
    //   }
    // }

    //   axios
    //     .put(
    //       `http://localhost:8000/edit_cart_item/${receiptNumber}/items/${itemName}/quantity`,
    //       obj
    //     )
    //     .then((response) => {
    //       console.log(response.data.message); // Success! Item quantity updated
    //     })
    //     .catch((error) => {
    //       console.error(error.response.data.detail); // Item not found in the cart
    //     });
    // } else {
    //   // Handle invalid input scenario
    //   console.error("Invalid quantity entered.");
    // }
  };

  function getRemainingItems(line_items, cart_items) {
    if (line_items === null || line_items === undefined) {
      console.log("line items is empty");
      return [];
    }

    if (cart_items === null || cart_items === undefined) {
      console.log("line items is not empty");
      return line_items;
    }

    return line_items.filter((line_item) => {
      return !cart_items.some(
        (cart_item) => cart_item.name === line_item.item_name
      );
    });
  }

  const saveConfirmedReceipts = (incoming_data) => {
    console.log("incoming data:", incoming_data);
    const conf = {
      receipt_number: incoming_data.receipt_number,
      note: String(incoming_data.note),
      status: "CONFIRMED",
      receipt_type: incoming_data.receipt_type,
      refund_for: String(incoming_data.note),
      order: String(incoming_data.order),
      created_at: incoming_data.created_at,
      updated_at: incoming_data.updated_at,
      source: incoming_data.source,
      receipt_date: incoming_data.receipt_date,
      cancelled_at: String(incoming_data["cancelled_at"]),
      total_money: incoming_data["total_money"],
      total_tax: incoming_data["total_tax"],
      points_earned: incoming_data["points_earned"],
      points_deducted: incoming_data["points_deducted"],
      points_balance: incoming_data["points_balance"],
      customer_id: String(incoming_data["customer_id"]),
      total_discount: incoming_data["total_discount"],
      employee_id: String(incoming_data["employee_id"]),
      store_id: incoming_data["store_id"],
      pos_device_id: incoming_data["pos_device_id"],
      dining_option: String(incoming_data["dining_option"]),
      total_discounts: incoming_data["total_discounts"],
      total_taxes: incoming_data["total_taxes"],
      tip: incoming_data["tip"],
      surcharge: incoming_data["surcharge"],
      line_items: incoming_data["line_items"],
      cart_items: incoming_data["cart_items"],
      line_items_total_qty: incoming_data["line_items_total_qty"],
      cart_items_total_qty: incoming_data["cart_items_total_qty"],
      grouped_items: ["empty"],
      missing_items: ["empty"],
    };

    axios
      .post("http://localhost:8000/saves_confirmed_receipts", conf)
      .then((response) => {
        console.log("Data saved successfully", conf);
        // Add your desired logic here
      })
      .catch((error) => {
        console.error("Error saving data:", error);
        // Handle any error that occurred during the request
      });
  };

  const handleConfirm = (missing_items) => {
    // console.log(">>>incoming g missing items:", missing_items);
    console.log("Confirm button clicked");
    console.log(
      "confirmed receipt to be saved:->:",
      receipt_data_prop.receipt_number
    );

    axios
      .get(
        `http://localhost:8000/get-cart_items_in_receipt/${receipt_data_prop.receipt_number}`
      )
      .then((response) => {
        console.log("Response", response.data.missing_items);

        if (!response.data.missing_items.length == 0) {
          saveConfirmedReceipts(response.data);
        } else {
          console.log("missing items is empty");

          const confirmed_receipt2 = {
            receipt_number: receipt_data_prop.receipt_number,
            note: String(receipt_data_prop.note),
            status: "CONFIRMED",
            receipt_type: receipt_data_prop.receipt_type,
            refund_for: String(receipt_data_prop.note),
            order: String(receipt_data_prop.order),
            created_at: receipt_data_prop.created_at,
            updated_at: receipt_data_prop.updated_at,
            source: receipt_data_prop.source,
            receipt_date: receipt_data_prop.receipt_date,
            cancelled_at: String(receipt_data_prop["cancelled_at"]),
            total_money: receipt_data_prop["total_money"],
            total_tax: receipt_data_prop["total_tax"],
            points_earned: receipt_data_prop["points_earned"],
            points_deducted: receipt_data_prop["points_deducted"],
            points_balance: receipt_data_prop["points_balance"],
            customer_id: String(receipt_data_prop["customer_id"]),
            total_discount: receipt_data_prop["total_discount"],
            employee_id: String(receipt_data_prop["employee_id"]),
            store_id: receipt_data_prop["store_id"],
            pos_device_id: receipt_data_prop["pos_device_id"],
            dining_option: String(receipt_data_prop["dining_option"]),
            total_discounts: receipt_data_prop["total_discounts"],
            total_taxes: receipt_data_prop["total_taxes"],
            tip: receipt_data_prop["tip"],
            surcharge: receipt_data_prop["surcharge"],
            line_items: receipt_data_prop["line_items"],
            cart_items: receipt_data_prop["cart_items"],
            line_items_total_qty: receipt_data_prop["line_items_total_qty"],
            cart_items_total_qty: receipt_data_prop["cart_items_total_qty"],
            grouped_items: receipt_data_prop["grouped_items"],
            missing_items: missing_items,
          };

          console.log("items on cart", response.data.cart_items);
          const conf = {
            receipt_number: response.data.receipt_number,
            note: String(response.data.note),
            status: "CONFIRMED",
            receipt_type: response.data.receipt_type,
            refund_for: String(response.data.note),
            order: String(response.data.order),
            created_at: response.data.created_at,
            updated_at: response.data.updated_at,
            source: response.data.source,
            receipt_date: response.data.receipt_date,
            cancelled_at: String(response.data["cancelled_at"]),
            total_money: response.data["total_money"],
            total_tax: response.data["total_tax"],
            points_earned: response.data["points_earned"],
            points_deducted: response.data["points_deducted"],
            points_balance: response.data["points_balance"],
            customer_id: String(response.data["customer_id"]),
            total_discount: response.data["total_discount"],
            employee_id: String(response.data["employee_id"]),
            store_id: response.data["store_id"],
            pos_device_id: response.data["pos_device_id"],
            dining_option: String(response.data["dining_option"]),
            total_discounts: response.data["total_discounts"],
            total_taxes: response.data["total_taxes"],
            tip: response.data["tip"],
            surcharge: response.data["surcharge"],
            line_items: response.data["line_items"],
            cart_items: response.data["cart_items"],
            line_items_total_qty: response.data["line_items_total_qty"],
            cart_items_total_qty: response.data["cart_items_total_qty"],
            grouped_items: ["empty"],
            missing_items: ["empty"],
          };

          console.log("new receipt:-", confirmed_receipt2);

          axios
            .post(
              "http://localhost:8000/saves_confirmed_receipts",
              confirmed_receipt2
            )
            .then((response) => {
              console.log("Data saved successfully-empty missing items");
              // Add your desired logic here
            })
            .catch((error) => {
              console.error("Error saving data -missing:", error);
              // Handle any error that occurred during the request
            });
        }
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });

    const comparisonData = array2.map((obj2, index) => {
      const isNameAvailable = array1.some(
        (obj1) => obj1.item_name === obj2.name
      );
      const isAgeAvailable = array1.some(
        (obj1) => obj1.quantity === obj2.quantity
      );
      const isCorrect = isNameAvailable && isAgeAvailable;

      return {
        index,
        name: obj2.name,
        quantity: obj2.quantity,
        isCorrect,
      };
    });

    console.log("comparison Data:", comparisonData.length);

    console.log("new receipt", receipt_data_prop);
  };

  const handleMissing = () => {
    if (missingItemsNames.length > 0) {
      console.log("Missing item names:", missingItemsNames);

      // Add your desired logic here
    }

    let grandTotalQuantityMatchinItems = 0;
    matchingItemsName.forEach((item) => {
      grandTotalQuantityMatchinItems += item.quantity;
    });

    let grandTotalForActualItems = 0;
    receipt_data_prop.line_items.forEach((item) => {
      grandTotalForActualItems += item.quantity;
    });

    console.log("matching items from handleMissing", matchingItemsName);
    console.log(
      "Grand total quantity from handleMissing:",
      grandTotalQuantityMatchinItems
    );
    console.log(
      "Grand total quantity for line_items from handleMissing:",
      grandTotalForActualItems
    );
  };

  const handleQuantityConditionforAdminButton = () => {
    if (missingItemsNames.length > 0) {
      console.log("Missing item names:", missingItemsNames);

      // Add your desired logic here
    }

    let grandTotalQuantityMatchinItems = 0;
    matchingItemsName.forEach((item) => {
      grandTotalQuantityMatchinItems += item.quantity;
    });

    let grandTotalForActualItems = 0;
    receipt_data_prop.line_items.forEach((item) => {
      grandTotalForActualItems += item.quantity;
    });

    console.log("matching items", matchingItemsName);
    console.log("Grand total quantity:", grandTotalQuantityMatchinItems);
    console.log(
      "Grand total quantity for line_items:",
      grandTotalForActualItems
    );
  };

  const handleConfirm2 = () => {
    // console.log(">>>incoming g missing items:", missing_items);
    console.log("Confirm button clicked");
    console.log(
      "confirmed receipt to be saved:->:",
      receipt_data_prop.receipt_number
    );

    axios
      .get(
        `http://localhost:8000/get-cart_items_in_receipt/${receipt_data_prop.receipt_number}`
      )
      .then((response) => {
        console.log("Response", response.data.missing_items);

        response.data.status = "CONFIRMED";

        console.log("modified status", response.data);
        let modifiedReceipt = response.data;
        console.debug(response.data);

        axios
          .post(
            "http://localhost:8000/saves_confirmed_receipts",
            modifiedReceipt
          )
          .then((response) => {
            console.log("Data saved successfully-empty missing items");
            // Add your desired logic here
          })
          .catch((error) => {
            console.error("Error saving data -missing:", error);
            // Handle any error that occurred during the request
          });
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };

  const renderComparison = (index) => {
    const obj2 = array2[index];
    const isNameAvailable = array1.some((obj1) => obj1.item_name === obj2.name);
    const isAgeAvailable = array1.some(
      (obj1) => obj1.quantity === obj2.quantity
    );
    const isCorrect = isNameAvailable && isAgeAvailable;
    const borderColor = isCorrect ? "green" : "red";
    const textColor = isCorrect ? "black" : "red";
    let errorText = "";

    if (!isNameAvailable && !isAgeAvailable) {
      errorText = "Wrong name and quantity";
      missingItemsNames.push(obj2.name);
    } else if (!isNameAvailable) {
      errorText = `Wrong name ${obj2.name}`;
      missingItemsNames.push(obj2.name);
    } else if (!isAgeAvailable) {
      errorText = `Wrong quantity of ${obj2.quantity}`;
    }

    if (isCorrect) {
      matchingItemsName.push({ name: obj2.name, quantity: obj2.quantity });
    }

    //setTotalQuantity((prevTotal) => prevTotal + obj2.quantity);

    //console.log("missing items", missing_itemsarr.length);
    //console.log("matching items", matchingItemsName);

    let grandTotalQuantityMatchinItems = 0;
    matchingItemsName.forEach((item) => {
      grandTotalQuantityMatchinItems += item.quantity;
    });

    let grandTotalForActualItems = 0;
    receipt_data_prop.line_items.forEach((item) => {
      grandTotalForActualItems += item.quantity;
    });

    console.log("matching items", matchingItemsName);
    // console.log("remaining items", remainingItems);

    // console.log("missing items", missingItemsNames);
    //setMissingItemsNames(missingItemsNames);
    console.log(
      "Grand total quantity for matching items:",
      grandTotalQuantityMatchinItems
    );
    console.log(
      "Grand total quantity for line_items:",
      grandTotalForActualItems
    );

    //console.log("receipt content", receipt_data_prop);

    // console.log(
    //   "remaining g_items:",
    //   getRemainingItems(data, matchingItemsName)
    // );
    // console.log("g items status:", getRemainingItems(data, matchingItemsName));

    if (isGrouped) {
      missingGroupItems = groupedItems.map((item) => {
        return {
          item_name: item.item_name,
          quantity: item.quantity,
        };
      });

      console.log("receipt is  grouped:-", missingItemsNames);

      console.log(
        "missing_items for grouped:",
        getRemainingItems(missingGroupItems, matchingItemsName)
      );

      console.log("missing items for grouped-->", missingItemsNames);

      //console.log("matching items for grouped", missingGroupItems);

      saveMissingItems(
        getRemainingItems(missingGroupItems, matchingItemsName),
        receipt_data_prop
      );
    } else if (!isGrouped) {
      console.log("receipt is not grouped");
      missingItemsNames = getRemainingItems(
        missingGroupItems,
        matchingItemsName
      );

      saveMissingItems(
        getRemainingItems(receipt_data_prop.line_items, matchingItemsName),
        receipt_data_prop
      );
    }

    console.log("missing items saved for non grouped  receipts");

    // if(getRemainingItems(receipt_data_prop.line_items, matchingItemsName).length !=0 ){
    //   console.log("confirm button is disabled")
    //   isAdminButtonDisabled=true

    // }

    if (
      grandTotalQuantityMatchinItems > grandTotalForActualItems ||
      hasRedBorder === true
    ) {
      console.log("total qty for matching is more than receipt items");
      //setIsAdminButtonDisabled(false);
      isAdminButtonDisabled = true;
    } else if (
      grandTotalQuantityMatchinItems < grandTotalForActualItems ||
      hasRedBorder === false
    ) {
      console.log("total qty for matching is less than receipt items");
      isAdminButtonDisabled = false;
    }

    return (
      <li key={index}>
        <div
          style={{
            border: `3px solid ${borderColor}`,
            borderRadius: "10px",
            padding: "10px",
            marginBottom: "10px",
          }}
        >
          <span style={{ color: textColor }}>
            {isCorrect
              ? `Correct item name: ${obj2.name}`
              : `Wrong ${errorText} - Take Action!!!`}

            <Button
              onClick={() =>
                updateItemQuantity(receipt_data_prop, obj2.name, obj2.quantity)
              }
            >
              {" "}
              Edit
            </Button>
            <Button
              onClick={() =>
                handleDeleteItem(receipt_data_prop.receipt_number, obj2.name)
              }
            >
              {" "}
              Delete
            </Button>

            {/* <input type="text" value={quantity} onChange={(e) => setQuantity(e.target.value)} /> */}

            {/* <button type="submit">Update Quantity</button> */}
          </span>
        </div>
      </li>
    );
  };

  const renderComparisons = () => {
    const comparisons = array2.map((_, index) => renderComparison(index));
    return <ul>{comparisons}</ul>;
  };

  const hasRedBorder = array2.some((_, index) => {
    const obj2 = array2[index];
    const isNameAvailable = array1.some((obj1) => obj1.item_name === obj2.name);
    const isAgeAvailable = array1.some(
      (obj1) => obj1.quantity === obj2.quantity
    );
    return !(isNameAvailable && isAgeAvailable);
  });

  return (
    <div>
      {renderComparisons()}

      <Button onClick={() => handleConfirm2()}>Confirm</Button>

      {/* <Button onClick={handleMissing}>check missing</Button> */}
    </div>
  );
}

export default CompareArrays;
