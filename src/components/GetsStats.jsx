import axios from "axios";
import React, { useState } from "react";

export const getSTats = async (r_number) => {
  //items.missing_items = quantity;
  const url = `http://localhost:8000/get-receipt-stats/${r_number}`;

 await axios
    .get(url)
    .then((response) => {
      // Handle the response if needed
      console.log(response.data.cart_items.length);
    })
    .catch((error) => {
      // Handle the error if the PUT request fails
      console.error("Error sending PUT request:", error);
    });

  return response.data.cart_items.length;
};

// export default SaveMissingItems;
