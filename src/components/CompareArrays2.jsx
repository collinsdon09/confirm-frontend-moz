
import React, { useState } from "react";
// import _ from 'lodash';


const Comparison2 = () => {
  const array1 = [
    { quantity: 3, item_name: "Tom" },
    { quantity: 2, name: "Jane" },
  ];
  const array2 = [
    { quantity: 4, name: "Alice" },
    { quantity: 5, name: "Jane" },
  ];

  const [matchedItems, setMatchedItems] = useState([]);
  const [mismatchedItems, setMismatchedItems] = useState([]);



  const common = _.intersectionWith(array1, array2, _.isEqual);
const missing = _.differenceWith(array1, array2, _.isEqual);



console.log("common", common)
console.log("missing", missing)

  const handleComparison = () => {
    const matched = [];
    const mismatched = [];

    array2.forEach((item2) => {
      const matchedItem = array1.find(
        (item1) => item1.item_name === item2.name && item1.quantity === item2.quantity
      );
      if (matchedItem) {
        matched.push(matchedItem);
      } else {
        mismatched.push(item2);
      }
    });


    console.log(matched)

    setMatchedItems(matched);
    setMismatchedItems(mismatched);
  };

  // Perform the comparison on component mount
  React.useEffect(() => {
    handleComparison();
  }, []);

  return (
    <div>
      {/* <h2>Matched Items:</h2>
      {matchedItems.map((item) => (
        <p key={item.quantity}>
          Name: {item.name}, ID: {item.quantity} - Both correct
        </p>
      ))}

      <h2>Mismatched Items:</h2>
      {mismatchedItems.map((item) => {
        const matchedNameItem = array1.find((item1) => item1.item_name === item.name);
        const matchedIdItem = array1.find((item1) => item1.quantity === item.quantity);

        if (matchedNameItem && matchedIdItem) {
          return (
            <p key={item.quantity}>
              Name: {item.item_name}, ID: {item.quantity} - Name and ID both correct
            </p>
          );
        } else if (matchedNameItem) {
          return (
            <p key={item.quantity}>
              Name: {item.name}, ID: {item.quantity} - Name is correct, but ID is incorrect
            </p>
          );
        } else if (matchedIdItem) {
          return (
            <p key={item.quantity}>
              Name: {item.name}, ID: {item.quantity} - Name is incorrect, but ID is correct
            </p>
          );
        } else {
          return (
            <p key={item.quantity}>
              Name: {item.name}, ID: {item.quantity} - Name and ID both incorrect
            </p>
          );
        }
      })} */}
    </div>
  );
};

export default Comparison2;

