import { useState, useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";

import viteLogo from "/vite.svg";
// import "./App.css";
import { Heading, Text, Box, Flex } from "@chakra-ui/react";
import axios from "axios";
// import { useHistory } from "react-router-dom";
import myStyle from "../myStyle/Tabs1.css";
import { Tabs, TabList, TabPanels, Tab, TabPanel } from "@chakra-ui/react";
import ToggleColorMode from "./ToggleColormode";
import ReceiptCard from "./ReceiptCard";
import WebSocketComponent from "./Websockets";
import {
  Card,
  SimpleGrid,
  Button,
  CardHeader,
  CardBody,
  CardFooter,
} from "@chakra-ui/react";
import { FaShoppingBasket } from "react-icons/fa";
import { WarningIcon } from "@chakra-ui/icons";
import { getSTats } from "./GetsStats";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
} from "@chakra-ui/react";
import { fetchOpenReceiptsCorrection } from "../functions/FetchOpenReceipts";

function Tabz() {
  const [count, setCount] = useState(0);
  const [Opencards, setOpenCards] = useState([]);
  const [AutoConfirmedCards, setAutoConfirmedCards] = useState([]);
  const [Refundcards, setRefundCards] = useState([]);
  const [ConfirmedCards, setConfirmedCards] = useState([]);
  const [cartItemNumber, setCartItemsNumber] = useState(0);
  const [totalqtyLineItems, setLineItems] = useState(0);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [autoconfirmedCartItems, setautoconfirmedCartItems] = useState([]);
  const [refundCartItems, setrefundCartItems] = useState([]);
  const [confirmedCartItems, setConfirmedCartItems] = useState([]);
  const [confirmedMissingItems, setConfirmedMissingItems] = useState([]);

  const [isModal1Open, setIsModal1Open] = useState(false);
  const [isModal2Open, setIsModal2Open] = useState(false);

  var shouldUpdateIncomingReceipt = true;

  const navigate = useNavigate();

  useEffect(() => {
    fetchOpenReceipts();
    fetchConfirmedReceipts();
    fetchRefundReceipts();
    fetchAutoConfirmedReceipts();
    //fetchOpenReceiptsCorrection()
    //fetchAutoConfirmedReceipts()
  }, []);

  const handleModal1Open = () => {
    setIsModal1Open(true);
    const namesArray = [];
    //setLineItems(receipt_data.line_items);
    const url = `http://localhost:8000/get-cart_items_in_receipt/${cardData.receipt_number}`;
    axios
      .get(url)
      .then((response) => {
        console.log("Autoconfirmed items", response.data.line_items);
        setrefundCartItems(response.data.line_items);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const handleModal1Close = () => {
    setIsModal1Open(false);
  };

  const handleModal2Open = () => {
    setIsModal2Open(true);
  };

  const handleModal2Close = () => {
    setIsModal2Open(false);
  };

  const handleAutoConfirmedClick = (cardData) => {
    //navigate("/receipt", { state: cardData });
    onOpen();

    const namesArray = [];
    //setLineItems(receipt_data.line_items);
    const url = `http://localhost:8000/get-cart_items_in_auto_confirmed/${cardData.receipt_number}`;
    axios
      .get(url)
      .then((response) => {
        console.log("Autoconfirmed items", response.data.line_items);
        setautoconfirmedCartItems(response.data.line_items);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const handleRefundClick = (cardData) => {
    //navigate("/receipt", { state: cardData });
    setIsModal1Open(true);

    const namesArray = [];
    //setLineItems(receipt_data.line_items);
    const url = `http://localhost:8000/get-cart_items_in_refund/${cardData.receipt_number}`;
    axios
      .get(url)
      .then((response) => {
        console.log("refund items", response.data.line_items);
        setrefundCartItems(response.data.line_items);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const handleConfirmedClick = (cardData) => {
    //navigate("/receipt", { state: cardData });
    setIsModal2Open(true);

    const namesArray = [];
    //setLineItems(receipt_data.line_items);
    const url = `http://localhost:8000/get-cart_items_in_confirmed/${cardData.receipt_number}`;
    axios
      .get(url)
      .then((response) => {
        console.log("confirmed items", response.data.line_items);
        setConfirmedCartItems(response.data.line_items);
        setConfirmedMissingItems(response.data.missing_items);
        console.log("missing items:->", response.data.missing_items)
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

   const fetchOpenReceipts = async () => {
    try {
      const response = await axios.get(
        // "http://localhost:8000/api/get-receipts-type",
        "http://localhost:8000/api/get-open-receipts-type",
        {
          //params: { status: "OPEN" }, // Replace "OPEN" with the desired status value
          params: ["OPEN"],
        }
      );
      //setReceipts(response.data);
      //console.log("res:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      console.log("UNIQUE:-->", uniqueArray);

      uniqueArray.forEach(async (obj) => {
        //console.log("receipt number for the cart:","", item.receipt_number)
        //console.log("cart", obj.cart_items.length);
        //console.log(obj.receipt_number);

        //console.log("stats",obj.receipt_number)
        setOpenCards(uniqueArray);

        if (obj.status === "IN-PROGRESS") {
          //console.log("------STATUS IS IN-PROGRESS");

          setOpenCards(uniqueArray);
          shouldUpdateIncomingReceipt = false;
        } else {
          //console.log("*******STATUS  IS NOT IN-PROGRESS");
          if (obj.cart_items.length >= 1) {
            console.log("check status", shouldUpdateIncomingReceipt);
            // console.log(
            //   "cart items for",
            //   obj.receipt_number,
            //   "cart item",
            //   obj.cart_items.length
            // );

            const inprogress_receipt = {
              receipt_number: obj.receipt_number,
              note: toString(obj.note),
              status: "IN-PROGRESS",
              receipt_type: obj.receipt_type,
              refund_for: toString(obj.note),
              order: toString(obj.order),
              created_at: obj.created_at,
              updated_at: obj.updated_at,
              source: obj.source,
              receipt_date: obj.receipt_date,
              cancelled_at: toString(obj["cancelled_at"]),
              total_money: obj["total_money"],
              total_tax: obj["total_tax"],
              points_earned: obj["points_earned"],
              points_deducted: obj["points_deducted"],
              points_balance: obj["points_balance"],
              customer_id: toString(obj["customer_id"]),
              total_discount: obj["total_discount"],
              employee_id: toString(obj["employee_id"]),
              store_id: obj["store_id"],
              pos_device_id: obj["pos_device_id"],
              dining_option: toString(obj["dining_option"]),
              total_discounts: obj["total_discounts"],
              total_taxes: obj["total_taxes"],
              tip: obj["tip"],
              surcharge: obj["surcharge"],
              line_items: obj["line_items"],
              cart_items: obj["cart_items"],
              missing_items: obj["missing_items"],
              line_items_total_qty: obj["line_items_total_qty"],
              cart_items_total_qty: obj["cart_items_total_qty"],
            };

            const inprogress_receipt2 = {
              receipt_number: obj.receipt_number,
              note: toString(obj.note),
              status: "IN-PROGRESS",
              receipt_type: obj.receipt_type,
              refund_for: toString(obj.note),
              order: toString(obj.order),
              created_at: obj.created_at,
              updated_at: obj.updated_at,
              source: obj.source,
              receipt_date: obj.receipt_date,
              cancelled_at: toString(obj.cancelled_at),
              total_money: obj.total_money,
              total_tax: obj.total_money,
              points_earned: obj.points_earned,
              points_deducted: obj.points_deducted,
              points_balance: obj.points_balance,
              customer_id: toString(obj.customer_id),
              total_discount: obj["total_discount"],
              employee_id: toString(obj.total_discount),
              store_id: obj.store_id,
              pos_device_id: obj.pos_device_id,
              dining_option: toString(obj.dining_option),
              total_discounts: obj.total_discounts,
              total_taxes: obj.total_taxes,
              tip: obj.tip,
              surcharge: obj.surcharge,
              line_items: obj.line_items,
              cart_items: obj.cart_items,
              missing_items: obj.missing_items,
              line_items_total_qty: toString(obj.line_items_total_qty),
              cart_items_total_qty: toString(obj.cart_items_total_qty),
            };

            console.log("inprogress receipt detected", inprogress_receipt);

            try {
              await axios.put(
                `http://localhost:8000/update-receipt/${inprogress_receipt.receipt_number}`,
                inprogress_receipt
              );
              console.log("Receipt updated successfully");

              // Clear form inputs after successful update
              // setReceiptNumber('');
              // setStatus('');
            } catch (error) {
              console.error("Error updating receipt:", error);
            }
          } else {
          }
        }
      });
    } catch (error) {
      console.error(error);
    }
  };

  const fetchConfirmedReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-confirmed-receipts",
        {
          params: { status: "CONFIRMED" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log(" confirmed:", uniqueArray);

      // const filteredCards = uniqueArray.filter(
      //   (item) => item.total_money > 100
      // );
      //console.log("filtered",filteredCards);
      setConfirmedCards(uniqueArray);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const fetchRefundReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-receipts-refund",
        {
          params: { status: "REFUND" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log("unique confirmed:", uniqueArray);

      // const filteredCards = uniqueArray.filter(
      //   (item) => item.total_money > 100
      // );
      //console.log("filtered",filteredCards);
      setRefundCards(uniqueArray);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const fetchAutoConfirmedReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-auto-confirmed-receipts",
        {
          params: { status: "AUTO-CONFIRMED" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log(" auto-confirmed:", uniqueArray);

      // const filteredCards = uniqueArray.filter(
      //   (item) => item.total_money > 100
      // );
      //console.log("filtered",filteredCards);
      setAutoConfirmedCards(uniqueArray);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const handleCardClick = (cardData, func) => {
    navigate("/receipt", { state: cardData });

  };



  return (
    <>
      <WebSocketComponent />
      <ToggleColorMode />
      <Tabs variant="enclosed">
        <TabList>
          {/* <Tab>To Be confirmed</Tab>
          <Tab>Auto-Confirmed</Tab>
          <Tab>Refund</Tab>
          <Tab>Confirmed</Tab>
           */}

          <Tab fontWeight="extrabold">To Be confirmed</Tab>
          <Tab fontWeight="extrabold">Auto-Confirmed</Tab>
          <Tab fontWeight="extrabold">Refund</Tab>
          <Tab fontWeight="extrabold">Confirmed</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            >
              {Opencards.map((card, index) => {
                if (card.status === "OPEN" || card.status === "IN-PROGRESS") {
                  return (
                    <Card
                      className="card"
                      width="150px"
                      height="200px"
                      padding="5px"
                      key={index}
                      onClick={() => handleCardClick(card)}
                    >
                      {/* Card content */}
                      <CardHeader>
                        {/* <Heading size="lg" className="Heading">
                          {card.receipt_number}
                          {card.created_at.slice(11, 19)}
                   
                        </Heading> */}

                        <Heading size="md" className="Heading">
                          <div
                            style={{
                              display: "flex",
                              alignItems: "left",
                            }}
                          >
                            <span
                              style={{
                                marginRight: "6px", // Adjust the spacing between the elements
                              }}
                            >
                              {card.receipt_number}
                            </span>
                            <span>{card.created_at.slice(11, 19)}</span>
                          </div>
                        </Heading>
                      </CardHeader>
                      <CardBody>
                        {/* <Text>{card.customer_id}</Text> */}
                        {/* <Text>STATUS: {card.status}</Text> */}
                      </CardBody>
                      <div className="status-infor">
                        <FaShoppingBasket color="	#0000ff" />
                        <Text>
                          {/* {card.cart_items.length}/{card.line_items.length} */}
                         
                        </Text>
                      </div>

                      <CardFooter>
                        {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                        <div className="card-header">
                          <Button
                            className="btn"
                            //color="#FF0000"
                            color={
                              card.status === "IN-PROGRESS" ? "green" : "red"
                            }
                            bgColor="transparent"
                            fontWeight="800"
                            fontSize="xl"
                          >
                            <WarningIcon color="#FF0000" />
                            {card.status}
                          </Button>
                        </div>
                      </CardFooter>
                    </Card>
                  );
                }
                return null;
              })}
            </SimpleGrid>

          </TabPanel>
          <TabPanel>
            <p>Auto-confirmed Tab</p>
            {/* <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            > */}
              {AutoConfirmedCards.map((card, index) => {
                // if (card.status === "REFUND") {
                //console.log("AUTO CONFIRMED STATTUS", card.status);
                return (
                  <>
                    <Card
                      className="card"
                      width="full"
                      height="200px"
                      key={index}
                      onClick={() => handleAutoConfirmedClick(card)}
                    >
                      {/* Card content */}
                      <CardHeader>
                        {/* <Stats receipt_props={card.receipt_number} /> */}
                        <Heading size="sm" className="Heading">
                          <div
                            style={{
                              display: "flex",
                              alignItems: "left",
                            }}
                          >
                            <span
                              style={{
                                marginRight: "6px", // Adjust the spacing between the elements
                              }}
                            >
                              {card.receipt_number}
                            </span>
                            <span>{card.created_at.slice(11, 19)}</span>
                          </div>
                        </Heading>
                      </CardHeader>
                      <CardBody>
                        <Text>card.customer_id</Text>
                        {/* <Text>STATUS: {card.status}</Text> */}
                      </CardBody>
                      <div className="status-infor">
                        <FaShoppingBasket color="	#0000ff" />
                        <Text>0/15</Text>
                      </div>

                      <CardFooter>
                        {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                        <div className="card-header">
                          <Button
                            className="btn"
                            color="#FF0000"
                            bgColor="transparent"
                            fontWeight="800"
                            fontSize="xl"
                          >
                            <WarningIcon color="#FF0000" />
                            {card.status}
                          </Button>
                        </div>
                      </CardFooter>
                    </Card>

                    {/* AUTO CONFIRMED MODAL */}
                    <Modal isOpen={isOpen} onClose={onClose}>
                      <ModalOverlay />
                      <ModalContent>
                        <ModalHeader>
                          AutoConfirmed Line Items for: {card.receipt_number}
                        </ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                          {/* <CompareArrays
                            array1={lineItems}
                            array2={cartItems}
                            groupedItems={groupedItems}
                            receipt_data_prop={receipt_data}
                          /> */}

                          <ul style={{ display: "flex", flexWrap: "wrap" }}>
                            {autoconfirmedCartItems.map((item) => (
                              <li
                                style={{
                                  border: `3px solid green`,
                                  borderRadius: "10px",
                                  padding: "10px",
                                  marginBottom: "10px",
                                }}
                                key={item.id}
                              >
                                <h1 style={{ marginRight: "10px" }}>
                                  Item name{item.item_name}
                                </h1>
                                <p> Qty: {item.quantity}</p>
                              </li>
                            ))}
                          </ul>
                        </ModalBody>
                      </ModalContent>
                    </Modal>
                  </>
                );
              })}
            {/* </SimpleGrid> */}
          </TabPanel>

          <TabPanel>
            <p>Refund</p>

            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            >
              {Refundcards.map((card, index) => {
                if (card.status === "REFUND") {
                  return (
                    <>
                      <Card
                        className="card"
                        width="full"
                        height="200px"
                        key={index}
                        onClick={() => handleRefundClick(card)}
                      >
                        {/* Card content */}
                        <CardHeader>
                          {/* <Stats receipt_props={card.receipt_number} /> */}
                          <Heading size="sm" className="Heading">
                            <div
                              style={{
                                display: "flex",
                                alignItems: "left",
                              }}
                            >
                              <span
                                style={{
                                  marginRight: "6px", // Adjust the spacing between the elements
                                }}
                              >
                                {card.receipt_number}
                              </span>{" "}
                              <span>{card.created_at.slice(11, 19)}</span>
                            </div>
                          </Heading>
                        </CardHeader>
                        <CardBody>
                          <Text>card.customer_id</Text>
                          {/* <Text>STATUS: {card.status}</Text> */}
                        </CardBody>
                        <div className="status-infor">
                          <FaShoppingBasket color="	#0000ff" />
                          <Text>0/15</Text>
                        </div>

                        <CardFooter>
                          {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                          <div className="card-header">
                            <Button
                              className="btn"
                              color="#FF0000"
                              bgColor="transparent"
                              fontWeight="800"
                              fontSize="xl"
                            >
                              <WarningIcon color="#FF0000" />
                              {card.status}
                            </Button>
                          </div>
                        </CardFooter>
                      </Card>

                      {/* REFUND MODAL */}
                      <Modal isOpen={isModal1Open} onClose={handleModal1Close}>
                        <ModalOverlay />
                        <ModalContent>
                          <ModalHeader>
                            Refund Items for: {card.receipt_number}
                          </ModalHeader>
                          <ModalCloseButton />
                          <ModalBody>
                            {/* <CompareArrays
                            array1={lineItems}
                            array2={cartItems}
                            groupedItems={groupedItems}
                            receipt_data_prop={receipt_data}
                          /> */}

                            <ul style={{ display: "flex", flexWrap: "wrap" }}>
                              {refundCartItems.map((item) => (
                                <li
                                  style={{
                                    border: `3px solid green`,
                                    borderRadius: "10px",
                                    padding: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={item.id}
                                >
                                  <h1 style={{ marginRight: "10px" }}>
                                    Item name{item.item_name}
                                  </h1>
                                  <p> Qty: {item.quantity}</p>
                                </li>
                              ))}
                            </ul>
                          </ModalBody>
                        </ModalContent>
                      </Modal>
                    </>
                  );
                }
                return null;
              })}
            </SimpleGrid>
          </TabPanel>

          <TabPanel>
            confirmed
            <SimpleGrid
              spacing={5}
              //templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
              className="container"
            >
              {ConfirmedCards.map((card, index) => {
                if (card.status === "CONFIRMED") {
                  return (
                    <>
                      <Card
                        className="card"
                        width="full"
                        height="200px"
                        key={index}
                        onClick={() => handleConfirmedClick(card)}
                      >
                        {/* Card content */}
                        <CardHeader>
                          {/* <Stats receipt_props={card.receipt_number} /> */}
                          <Heading size="sm" className="Heading">
                            <div
                              style={{
                                display: "flex",
                                alignItems: "left",
                              }}
                            >
                              <span
                                style={{
                                  marginRight: "6px", // Adjust the spacing between the elements
                                }}
                              >
                                {card.receipt_number}
                              </span>
                              <span>{card.created_at.slice(11, 19)}</span>
                            </div>
                          </Heading>
                        </CardHeader>
                        <CardBody>
                          <Text>card.customer_id</Text>
                          {/* <Text>STATUS: {card.status}</Text> */}
                        </CardBody>
                        <div className="status-infor">
                          <FaShoppingBasket color="	#0000ff" />
                          <Text>0/15</Text>
                        </div>

                        <CardFooter>
                          {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                          <div className="card-header">
                            <Button
                              className="btn"
                              color="#FF0000"
                              bgColor="transparent"
                              fontWeight="800"
                              fontSize="xl"
                            >
                              <WarningIcon color="#FF0000" />
                              {card.status}
                            </Button>
                          </div>
                        </CardFooter>
                      </Card>

                      {/* CONFIRMED MODAL */}
                      {/* REFUND MODAL */}
                      <Modal isOpen={isModal2Open} onClose={handleModal2Close}>
                        <ModalOverlay />
                        <ModalContent>
                          <ModalHeader>
                            Confirmed Items for {card.receipt_number}
                          </ModalHeader>
                          <ModalCloseButton />
                          <ModalBody>
                            {/* <CompareArrays
                            array1={lineItems}
                            array2={cartItems}
                            groupedItems={groupedItems}
                            receipt_data_prop={receipt_data}
                          /> */}
                            <p>Cart Items</p>
                            <ul style={{ display: "flex", flexWrap: "wrap" }}>
                              {confirmedCartItems.map((item) => (
                                <li
                                  style={{
                                    border: `3px solid green`,
                                    borderRadius: "10px",
                                    padding: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={item.id}
                                >
                                  <h1 style={{ marginRight: "10px" }}>
                                    Item name{item.item_name}
                                  </h1>
                                  <p> Qty: {item.quantity}</p>
                                </li>
                              ))}
                            </ul>

                            <p>missing items</p>
                            <ul style={{ display: "flex", flexWrap: "wrap" }}>
                              {/* {confirmedMissingItems.map((item) => (
                                <li
                                  style={{
                                    border: `3px solid green`,
                                    borderRadius: "10px",
                                    padding: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={item.id}
                                >
                                  <h1 style={{ marginRight: "10px" }}>
                                    Item name{item.item_name}
                                  </h1>
                                  <p> Qty: {item.quantity}</p>
                                </li>
                              ))} */}
                            </ul>
                          </ModalBody>
                        </ModalContent>
                      </Modal>
                    </>
                  );
                }
                return null;
              })}
            </SimpleGrid>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  );
}

export default Tabz;
