import { useState } from "react";
// var data = require("data.json");

const data = {
  names: [
    { name: "John Doe" },
    { name: "Alice Johnson" },
    { name: "Michael Smith" },
    { name: "Emily Davis" },
    { name: "Daniel Brown" },
    { name: "Olivia Taylor" },
    { name: "Christopher Clark" },
    { name: "Sophia Martinez" },
    { name: "Matthew Anderson" },
    { name: "Isabella Wilson" },
    { name: "David Miller" },
    { name: "Ava Moore" },
    { name: "James Thompson" },
    { name: "Emma Hernandez" },
    { name: "Joseph Walker" },
    { name: "Mia Lewis" },
    { name: "Andrew Green" },
    { name: "Charlotte Hill" },
    { name: "William Adams" },
    { name: "Abigail Turner" },
  ],
};

function SearchComponent() {
  const [value, setValue] = useState("");

  const onChange = (event) => {
    setValue(event.target.value);
  };

  const onSearch = (searchTerm) => {
    setValue(searchTerm);
    //api to fetch the search results
    console.log("searched", searchTerm);
  };
  return (
    <div>
      <h1>Search</h1>
      <div className="search-container">
        <div className="search-inner">
          <input type="text" value={value} onChange={onChange} />
          <button onClick={() => onSearch(value)}>Search Text</button>
        </div>

        <div className="dropdown">
          {data.names
            .filter((item) => {
              const searchTerm = value.toLowerCase();
              const fullName = item.name.toLowerCase();

              return (
                searchTerm &&
                fullName.startsWith(searchTerm) &&
                fullName !== searchTerm
              );
            }).slice(0,5)

            .map((item, index) => (
              <div
                onClick={() => onSearch(item.name)}
                className="dropdown-row"
                key={index}
              >
                {item.name}
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

export default SearchComponent;
