import axios from 'axios';

export const fetchOpenReceiptsCorrection = async () => {
  try {
    const response = await axios.get(
      // "http://localhost:8000/api/get-receipts-type",
      "http://localhost:8000/api/get-open-receipts-type",
      {
        //params: { status: "OPEN" }, // Replace "OPEN" with the desired status value
        params: ["OPEN"],
      }
    );
    
    const dataArray = Array.from(response.data);

    const uniqueArray = Object.values(
      dataArray.reduce((uniqueItems, currentItem) => {
        const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

        if (!uniqueItems[key]) {
          uniqueItems[key] = currentItem;
        }

        return uniqueItems;
      }, {})
    );

    return uniqueArray; // Return the uniqueArray or any other data you want to expose

  } catch (error) {
    // Handle any errors that occur during the API request
    console.error(error);
    throw error;
  }
};
