// import React, { useState, useEffect } from "react";
// import _ from "lodash";
// import { Button } from "@chakra-ui/react";

// export default function CartItemValidity({
//   receipt_props,
//   lineItems,
//   groupedItems,
//   cartItems,
// }) {
//   const [isAdminButtonDisabled, setIsAdminButtonDisabled] = useState(true);
//   const [isConfirmButtonDisabled, setIsConfirmButtonDisabled] = useState(false);
//   const [matchingItems, setMatchingItems] = useState([]);

//   useEffect(() => {
//     let correctItemCount = 0;
//     const matchingItems = cartItems.map((cartItem) => {
//       if (receipt_props.grouped_items.length === 0) {
//         const lineItem = _.find(lineItems, { item_name: cartItem.name });

//         if (lineItem) {
//           let output = `Correct item name: ${cartItem.name}`;

//           if (cartItem.quantity === lineItem.quantity) {
//             output += ` Correct qty ${cartItem.quantity}`;
//             correctItemCount++;
//             return { name: cartItem.name, quantity: cartItem.quantity };
//           } else {
//             output += ` Incorrect qty ${cartItem.quantity}`;
//             setIsConfirmButtonDisabled(true);
//             return null;
//           }
//         } else {
//           setIsConfirmButtonDisabled(true);
//           return null;
//         }
//       } else if (receipt_props.grouped_items.length > 0) {
//         const groupItems = _.filter(receipt_props.grouped_items, {
//           item_name: cartItem.name,
//         });

//         if (groupItems.length > 0) {
//           let output = `Correct item name: ${cartItem.name}`;

//           const matchingItem = _.find(groupItems, {
//             quantity: cartItem.quantity,
//           });

//           if (matchingItem) {
//             output += ` Correct qty ${cartItem.quantity}`;
//             correctItemCount++;
//             return { name: cartItem.name, quantity: cartItem.quantity };
//           } else {
//             output += ` Incorrect qty ${cartItem.quantity}`;
//             return null;
//           }
//         } else {
//           return null;
//         }
//       }
//     });

//     setIsAdminButtonDisabled(correctItemCount === 0);
//     setIsConfirmButtonDisabled(correctItemCount === 0);
//     setMatchingItems(matchingItems.filter((item) => item !== null));
//   }, [cartItems, lineItems, receipt_props.grouped_items]);

//   return (
//     <div>
//       <h2>Results:</h2>
//       <ul>
//         {cartItems.map((cartItem, index) => (
//           <li
//             key={index}
//             style={{
//               background: matchingItems.some(
//                 (item) =>
//                   item.name === cartItem.name && item.quantity === cartItem.quantity
//               )
//                 ? "green"
//                 : "red",
//               color: "black",
//               padding: "5px",
//               margin: "5px",
//             }}
//           >
//             {`Item name: ${cartItem.name}, Quantity: ${cartItem.quantity}`}
//           </li>
//         ))}
//       </ul>
//       <p>Correct Item Count: {matchingItems.length}</p>
//       <Button isDisabled={isAdminButtonDisabled}>Admin</Button>
//       <Button isDisabled={isConfirmButtonDisabled}>Confirm</Button>
//     </div>
//   );
// }

// import React, { useState, useEffect } from "react";
// import _ from "lodash";
// import { Button } from "@chakra-ui/react";

// export default function CartItemValidity({
//   receipt_props,
//   lineItems,
//   groupedItems,
//   cartItems,
// }) {
//   const [isAdminButtonDisabled, setIsAdminButtonDisabled] = useState(true);
//   const [isConfirmButtonDisabled, setIsConfirmButtonDisabled] = useState(false);
//   const [matchingItems, setMatchingItems] = useState([]);

//   useEffect(() => {
//     let correctItemCount = 0;
//     const matchingItems = cartItems.map((cartItem) => {
//       if (receipt_props.grouped_items.length === 0) {
//         const lineItem = _.find(lineItems, { item_name: cartItem.name });

//         if (lineItem) {
//           let output = `Correct item name: ${cartItem.name}`;

//           if (cartItem.quantity === lineItem.quantity) {
//             output += ` Correct qty ${cartItem.quantity}`;
//             correctItemCount++;
//             return { name: cartItem.name, quantity: cartItem.quantity };
//           } else {
//             output += ` Incorrect qty ${cartItem.quantity}`;
//             setIsConfirmButtonDisabled(true);
//             return null;
//           }
//         } else {
//           setIsConfirmButtonDisabled(true);
//           return null;
//         }
//       } else if (receipt_props.grouped_items.length > 0) {
//         const groupItems = _.filter(receipt_props.grouped_items, {
//           item_name: cartItem.name,
//         });

//         if (groupItems.length > 0) {
//           let output = `Correct item name: ${cartItem.name}`;

//           const matchingItem = _.find(groupItems, {
//             quantity: cartItem.quantity,
//           });

//           if (matchingItem) {
//             output += ` Correct qty ${cartItem.quantity}`;
//             correctItemCount++;
//             return { name: cartItem.name, quantity: cartItem.quantity };
//           } else {
//             output += ` Incorrect qty ${cartItem.quantity}`;
//             return null;
//           }
//         } else {
//           return null;
//         }
//       }
//     });

//     setIsAdminButtonDisabled(correctItemCount === 0);
//     setIsConfirmButtonDisabled(correctItemCount === 0);
//     setMatchingItems(matchingItems.filter((item) => item !== null));

//     // Log matching items to the console
//     console.log("Matching Items:", matchingItems.filter((item) => item !== null));
//   }, [cartItems, lineItems, receipt_props.grouped_items]);

//   return (
//     <div>
//       <h2>Results:</h2>
//       <ul>
//         {cartItems.map((cartItem, index) => (
//           <li
//             key={index}
//             style={{
//               background: matchingItems.some(
//                 (item) =>
//                   item.name === cartItem.name && item.quantity === cartItem.quantity
//               )
//                 ? "green"
//                 : "red",
//               color: "black",
//               padding: "5px",
//               margin: "5px",
//             }}
//           >
//             {`Item name: ${cartItem.name}, Quantity: ${cartItem.quantity}`}
//           </li>
//         ))}
//       </ul>
//       <p>Correct Item Count: {matchingItems.length}</p>
//       <Button isDisabled={isAdminButtonDisabled}>Admin</Button>
//       <Button isDisabled={isConfirmButtonDisabled}>Confirm</Button>
//     </div>
//   );
// }


//////////////////////////////////////////////////////////////////////////////////
// import React, { useState, useEffect } from "react";
// import _ from "lodash";
// import { Button } from "@chakra-ui/react";
// import axios from "axios"; // Import axios library for HTTP requests


// function get_remaining(items) {
//   // Perform actions with matchingItems
//   console.log("Matching Items:", items);

//   // Get remaining items
//   const remainingItems = lineItems.filter((lineItem) => {
//     return !items.some(
//       (item) =>
//         item.name === lineItem.item_name && item.quantity === lineItem.quantity
//     );
//   });

//   console.log("remaiining items", remainingItems);
// }

// export default function CartItemValidity({
//   receipt_props,
//   lineItems,
//   groupedItems,
//   cartItems,
// }) {
//   const [isAdminButtonDisabled, setIsAdminButtonDisabled] = useState(true);
//   const [isConfirmButtonDisabled, setIsConfirmButtonDisabled] = useState(false);
//   const [matchingItems, setMatchingItems] = useState([]);

//   useEffect(() => {
//     let correctItemCount = 0;
//     const matchingItems = cartItems.map((cartItem) => {
//       if (receipt_props.grouped_items.length === 0) {
//         const lineItem = _.find(lineItems, { item_name: cartItem.name });

//         if (lineItem) {
//           let output = `Correct item name: ${cartItem.name}`;

//           if (cartItem.quantity === lineItem.quantity) {
//             output += ` Correct qty ${cartItem.quantity}`;
//             correctItemCount++;
//             return { name: cartItem.name, quantity: cartItem.quantity };
//           } else {
//             output += ` Incorrect qty ${cartItem.quantity}`;
//             setIsConfirmButtonDisabled(true);
//             return null;
//           }
//         } else {
//           setIsConfirmButtonDisabled(true);
//           return null;
//         }
//       } else if (receipt_props.grouped_items.length > 0) {
//         const groupItems = _.filter(receipt_props.grouped_items, {
//           item_name: cartItem.name,
//         });

//         if (groupItems.length > 0) {
//           let output = `Correct item name: ${cartItem.name}`;

//           const matchingItem = _.find(groupItems, {
//             quantity: cartItem.quantity,
//           });

//           if (matchingItem) {
//             output += ` Correct qty ${cartItem.quantity}`;
//             correctItemCount++;
//             return { name: cartItem.name, quantity: cartItem.quantity };
//           } else {
//             output += ` Incorrect qty ${cartItem.quantity}`;
//             return null;
//           }
//         } else {
//           return null;
//         }
//       }
//     });

//     setIsAdminButtonDisabled(correctItemCount === 0);
//     setIsConfirmButtonDisabled(correctItemCount === 0);
//     setMatchingItems(matchingItems.filter((item) => item !== null));

//     // Log matching items to the console
//     console.log(
//       "Matching Items:",
//       matchingItems.filter((item) => item !== null)
//     );

//     // Log items present in lineItems but not in matchingItems
//     //   const notMatchingItems = _.difference(lineItems, matchingItems, "name");
//     //   console.log("Not Matching Items:", notMatchingItems);
//     // }, [cartItems, lineItems, receipt_props.grouped_items]);

//     // console.log("Remaining Items:", remainingItems);

//     const notMatchingItems = _.difference(lineItems, matchingItems, "item_name");
//     console.log("Not Matching Items:", notMatchingItems);

//     // Perform PUT request to push missing items to the server
//     axios
//       .put(`http://localhost:8000/add_missing_items_to_main_receipt/${receipt_props.receipt_number}`, notMatchingItems)
//       .then((response) => {
//         console.log("Missing items pushed to server:", response.data);
//         // Handle successful response if needed
//       })
//       .catch((error) => {
//         console.error("Error pushing missing items to server:", error);
//         // Handle error if needed
//       });
//   }, [cartItems, lineItems, receipt_props.grouped_items]);

//   return (
//     <div>
//       <h2>Results:</h2>
//       <ul>
//         {cartItems.map((cartItem, index) => (
//           <li
//             key={index}
//             style={{
//               background: matchingItems.some(
//                 (item) =>
//                   item.name === cartItem.name &&
//                   item.quantity === cartItem.quantity
//               )
//                 ? "green"
//                 : "red",
//               color: "black",
//               padding: "5px",
//               margin: "5px",
//             }}
//           >
//             {`Item name: ${cartItem.name}, Quantity: ${cartItem.quantity}`}
//           </li>
//         ))}
//       </ul>
//       <p>Correct Item Count: {matchingItems.length}</p>
//       <Button isDisabled={isAdminButtonDisabled}>Admin</Button>
//       <Button isDisabled={isConfirmButtonDisabled}>Confirm</Button>
//     </div>
//   );
// }




import React, { useState, useEffect } from "react";
import axios from "axios";
import _ from "lodash";
import { Button } from "@chakra-ui/react";

export default function CartItemValidity({
  receipt_props,
  lineItems,
  groupedItems,
  cartItems,
}) {
  const [isAdminButtonDisabled, setIsAdminButtonDisabled] = useState(true);
  const [isConfirmButtonDisabled, setIsConfirmButtonDisabled] = useState(
    false
  );
  const [matchingItems, setMatchingItems] = useState([]);
  const [notMatchingItems, setNotMatchingItems] = useState([]);

  useEffect(() => {
    let correctItemCount = 0;
    const matchingItems = cartItems.map((cartItem) => {
      if (receipt_props.grouped_items.length === 0) {
        const lineItem = _.find(lineItems, { item_name: cartItem.name });

        if (lineItem) {
          let output = `Correct item name: ${cartItem.name}`;

          if (cartItem.quantity === lineItem.quantity) {
            output += ` Correct qty ${cartItem.quantity}`;
            correctItemCount++;
            return { name: cartItem.name, quantity: cartItem.quantity };
          } else {
            output += ` Incorrect qty ${cartItem.quantity}`;
            setIsConfirmButtonDisabled(true);
            return null;
          }
        } else {
          setIsConfirmButtonDisabled(true);
          return null;
        }
      } else if (receipt_props.grouped_items.length > 0) {
        const groupItems = _.filter(receipt_props.grouped_items, {
          item_name: cartItem.name,
        });

        if (groupItems.length > 0) {
          let output = `Correct item name: ${cartItem.name}`;

          const matchingItem = _.find(groupItems, {
            quantity: cartItem.quantity,
          });

          if (matchingItem) {
            output += ` Correct qty ${cartItem.quantity}`;
            correctItemCount++;
            return { name: cartItem.name, quantity: cartItem.quantity };
          } else {
            output += ` Incorrect qty ${cartItem.quantity}`;
            return null;
          }
        } else {
          return null;
        }
      }
    });

    setIsAdminButtonDisabled(correctItemCount === 0);
    setIsConfirmButtonDisabled(correctItemCount === 0);
    setMatchingItems(matchingItems.filter((item) => item !== null));

    const updatedNotMatchingItems = _.difference(
      lineItems,
      matchingItems,
      "name"
    );
    setNotMatchingItems(updatedNotMatchingItems);
  }, [cartItems, lineItems, receipt_props.grouped_items]);

  const handleAddMatchingItem = (item) => {
    const updatedNotMatchingItems = notMatchingItems.map((notMatchingItem) => {
      if (
        notMatchingItem.name === item.name &&
        notMatchingItem.quantity >= item.quantity
      ) {
        return {
          ...notMatchingItem,
          quantity: notMatchingItem.quantity - item.quantity,
        };
      }
      return notMatchingItem;
    });

    setNotMatchingItems(updatedNotMatchingItems);
  };

  useEffect(() => {
    axios
      .put(`http://localhost:8000/add_missing_items_to_main_receipt/${receipt_props.receipt_number}`, notMatchingItems)
      .then((response) => {
        console.log("Missing items pushed to server:", response.data);
        // Handle successful response if needed
      })
      .catch((error) => {
        console.error("Error pushing missing items to server:", error);
        // Handle error if needed
      });
  }, [notMatchingItems]);

  return (
    <div>
      <h2>Results:</h2>
      <ul>
        {cartItems.map((cartItem, index) => (
          <li
            key={index}
            style={{
              background: matchingItems.some(
                (item) =>
                  item.name === cartItem.name &&
                  item.quantity === cartItem.quantity
              )
                ? "green"
                : "red",
              color: "black",
              padding: "5px",
              margin: "5px",
            }}
          >
            {`Item name: ${cartItem.name}, Quantity: ${cartItem.quantity}`}
            <Button onClick={() => handleAddMatchingItem(cartItem)}>
              Add Matching Item
            </Button>
          </li>
        ))}
      </ul>
      <p>Correct Item Count: {matchingItems.length}</p>
      <Button isDisabled={isAdminButtonDisabled}>Admin</Button>
      <Button isDisabled={isConfirmButtonDisabled}>Confirm</Button>
    </div>
  );
}
