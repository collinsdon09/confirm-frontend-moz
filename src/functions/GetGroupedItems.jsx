import React from 'react';
import _ from 'lodash';

const getGroupedItems = (lineItems) => {
  return _.values(_.groupBy(lineItems, 'item_id')).map(items => ({
    ...items[0],
    quantity: _.sum(items, 'quantity'),
  }));
};

export default getGroupedItems;
