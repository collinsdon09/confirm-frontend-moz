import axios from "axios";
import React, { useState } from "react";

export const saveMissingItems = async (items, receipt_object) => {
  //items.missing_items = quantity;
  const url2 = `http://localhost:8000/add_missing_items_to_main_receipt/${receipt_object.receipt_number}`;

  console.log("receipt object for missing-->:", receipt_object)
  console.log(
    "missing items about to be saved:",

    "receipt#",
    items
  );




  return axios
    .put(url2, items)
    .then((response) => {
      // Handle the response if needed
      console.log(response.data)

    })
    .catch((error) => {
      // Handle the error if the PUT request fails
      console.error("Error sending PUT request:", error);
    });
};



// export default SaveMissingItems;


