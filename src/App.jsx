import { useState } from "react";
import viteLogo from "/vite.svg";
import "./App.css";

import Tabz from "./components/Tabs";
import { BrowserRouter, Routes, Route, Link, NavLink } from "react-router-dom";
import ReceiptCard from "./components/ReceiptCard";
import ReceiptView from "./pages/ReceiptView";
import { InstantSearch } from "react-instantsearch-core";
import { instantSearchConfig } from "./typesense";
import  TypesenseInstantSearchAdapter  from "typesense-instantsearch-adapter";
import SearchBox from "./pages/SearchBox";
import CustomComponent from "./components/Results"
import WebSocketComponent from "./components/Websockets";
import AutoConfirmedPage from "./pages/AutoconfirmedPage";
import RefundPage from "./pages/RefundPage";
import ConfirmedPage from "./pages/ConfirmedPage";





const INDEX_NAME = import.meta.env.VITE_APP_TYPESENSE_INDEX_NAME;
const typesenseInstantSearchAdapter = new TypesenseInstantSearchAdapter(
  instantSearchConfig
);


function App() {
  const [count, setCount] = useState(0);

  return (
    <BrowserRouter>
      <main>
        <Routes>

        {/* <Route path="/" element={<WebSocketComponent />} /> */}
      <Route path="/receipt" element={<ReceiptView />} />
         <Route path="/" element={<WebSocketComponent />} />
          <Route path="/receipt" element={<ReceiptView />} /> 
          <Route path="/auto-confirmed-view" element={<AutoConfirmedPage />} /> 
          <Route path="/refund-view" element={<RefundPage />} /> 
          <Route path="/confirmed-view" element={<ConfirmedPage />} /> 



          {/* <Route path="/confirmed_receipt" element={<ReceiptCard />} /> */}
        </Routes>
      </main>
    </BrowserRouter>



    // <div>
    //    <InstantSearch
    //       indexName={INDEX_NAME}
    //       searchClient={typesenseInstantSearchAdapter.searchClient}
    //     >
    //       <SearchBox />
    //       {/* <Results /> */}
    //       <CustomComponent/>
    //     </InstantSearch>
    // </div>
  );
}

export default App;
