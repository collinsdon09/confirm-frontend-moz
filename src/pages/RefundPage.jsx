import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import _ from "lodash";
import getGroupedItems from "../functions/GetGroupedItems";
import { saveGroupedItemsRefund } from "../functions/SaveGroupedItemsRefund";

const RefundPage = () => {
  const [updatedRefundData, setupdatedRefundData] = useState(null);
  const [data, setData] = useState(null);

  const location = useLocation();
  const cardData = location.state;
  console.log("refund card clicked", cardData);

  // const fetchData = (receipt_number) => {
  //   axios
  //     .get(`http://localhost:8000/get-cart_items_in_refund/${receipt_number}`)
  //     .then((response) => {
  //       console.log("Response", response.data.grouped_items);
  //       const cartItems = response.data.grouped_items.map((item) => ({
  //         id: item.id,
  //         item_name: item.item_name,
  //         quantity: item.quantity,
  //       }));
  //       setData(cartItems);
  //       // setMissingData(response.data.missing_items);
  //     })
  //     .catch((error) => {
  //       console.error("Error fetching data:", error);
  //     });
  // };


  const fetchData = (receipt_number) => {
    axios
      .get(`http://localhost:8000/get-cart_items_in_refund/${receipt_number}`)
      .then((response) => {
        console.log("total item quanity", response.data.line_items_qty  )
      
        if (response.data.grouped_items) {
          console.log("Response", response.data.grouped_items);
          const cartItems = response.data.grouped_items.map((item) => ({
            id: item.id,
            item_name: item.item_name,
            quantity: item.quantity,
          }));
          setData(cartItems);
          console.log("cartItems", cartItems)
          // setMissingData(response.data.missing_items);
        } else if(response.data.line_items) {
          console.error("No grouped_items available in the response");
          const cartItems2 = response.data.line_items.map((item) => ({
            id: item.id,
            item_name: item.item_name,
            quantity: item.quantity,
          }));
          //setData(cartItems);

          
          const totalQty = _.reduce(
            response.data.line_items,
            (accumulator, currentItem) => accumulator + currentItem.quantity,
            0
          );

          console.log("total qty:", totalQty)


        }
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };
  

  useEffect(() => {
    fetchData(cardData.receipt_number);
  }, []);

  // Check if cardData is empty
  if (!cardData) {
    return <p>No data available.</p>;
  }

  return (
    <>
      <p>STATUS: {cardData.status}</p>
      <p>R#: {cardData.receipt_number}</p>

      {data ? (
        <ul style={{ display: "flex", flexWrap: "wrap" }}>
          {data.map((item) => (
            <li
              style={{
                border: "3px solid green",
                borderRadius: "10px",
                padding: "10px",
                marginBottom: "10px",
              }}
              key={item.id}
            >
              <h1 style={{ marginRight: "10px" }}>
                Item name: {item.item_name}
              </h1>
              <p>Qty: {item.quantity}</p>
            </li>
          ))}
        </ul>
      ) : (
        <ul style={{ display: "flex", flexWrap: "wrap" }}>
          {cardData.line_items.map((item) => (
            <li
              style={{
                border: "3px solid green",
                borderRadius: "10px",
                padding: "10px",
                marginBottom: "10px",
              }}
              key={item.id}
            >
              <h1 style={{ marginRight: "10px" }}>
                Item name: {item.item_name}
              </h1>
              <p>Qty: {item.quantity}</p>
            </li>
          ))}
        </ul>
      )}
    </>
  );
};

export default RefundPage;
