// import { useLocation } from "react-router-dom";
// import { useEffect, useState } from "react";
// import axios from "axios";
// import _, { has } from "lodash";
// import { hasIn } from "lodash";
// import getGroupedItems from "../functions/GetGroupedItems";

// const ConfirmedPage = () => {
//   //const [updatedRefundData, setUpdatedRefundData] = useState([]);
//   const [data, setData] = useState([]);
//   const [line_items, setLineData] = useState([]);
//   const [missingData, setMissingData] = useState([]);

//   const location = useLocation();
//   const cardData = location.state;
//   console.log("autconfirmed card clicked", cardData);
//   c

//   useEffect(() => {
//     fetchData();
//   }, []);

//   const fetchData = () => {
//     axios
//       .get(
//         `http://localhost:8000/get-cart_items_in_confirmed/${cardData.receipt_number}`
//       )
//       .then((response) => {
//         console.log("Response", response.data.grouped_items);
//         //setData(response.data.cart_items);

//         // response.data.cart_items.forEach((item) => {
//         //     console.log("item name", item.name)
//         // });


//         const cartItems = response.data.cart_items.map((item) => {
//             console.log("item name", item.name);
//           });
//         setData(cartItems);
//         setMissingData(response.data.missing_items);
//       })
//       .catch((error) => {
//         console.error("Error fetching data:", error);
//       });
//   };

//   // Check if cardData is empty
//   if (!cardData) {
//     return <p>No data available.</p>;
//   }

//   console.log("data:", data);

  

//   return (
//     <>
//       <p>STATUS: {cardData.status}</p>
//       <p>R#: {cardData.receipt_number}</p>

//       {data.length > 0 ? (
//         <>
//           <h2>Grouped Items:</h2>
//           <ul style={{ display: "flex", flexWrap: "wrap" }}>
//             {data.map((item) => (
//               <li
//                 style={{
//                   border: "3px solid green",
//                   borderRadius: "10px",
//                   padding: "10px",
//                   marginBottom: "10px",
//                 }}
//                 key={item.id}
//               >
//                 <h1 style={{ marginRight: "10px" }}>
//                   Item name: {item.item_name}
//                 </h1>
//                 <p>Qty: {item.quantity}</p>
//               </li>
//             ))}
//           </ul>
//         </>
//       ) : (
//         <>
//           <h2>Line Items:</h2>
//           <ul style={{ display: "flex", flexWrap: "wrap" }}>
//             {line_items.map((item) => (
//               <li
//                 style={{
//                   border: "3px solid green",
//                   borderRadius: "10px",
//                   padding: "10px",
//                   marginBottom: "10px",
//                 }}
//                 key={item.id}
//               >
//                 <h1 style={{ marginRight: "10px" }}>
//                   Item name: {item.item_name}
//                 </h1>
//                 <p>Qty: {item.quantity}</p>
//               </li>
//             ))}
//           </ul>
//         </>
//       )}

//       {missingData.length > 0 && (
//         <>
//           <h2>Missing Items:</h2>
//           <ul style={{ display: "flex", flexWrap: "wrap" }}>
//             {missingData.map((item) => (
//               <li
//                 style={{
//                   border: "3px solid red",
//                   borderRadius: "10px",
//                   padding: "10px",
//                   marginBottom: "10px",
//                 }}
//                 key={item.id}
//               >
//                 <h1 style={{ marginRight: "10px" }}>
//                   Item name: {item.item_name}
//                 </h1>
//                 <p>Qty: {item.quantity}</p>
//               </li>
//             ))}
//           </ul>
//         </>
//       )}
//     </>
//   );
// };

// export default ConfirmedPage;
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { hasIn } from "lodash";
import getGroupedItems from "../functions/GetGroupedItems";

const ConfirmedPage = () => {
  const [data, setData] = useState([]);
  const [line_items, setLineData] = useState([]);
  const [missingData, setMissingData] = useState([]);

  const location = useLocation();
  const cardData = location.state;
  console.log("autconfirmed card clicked", cardData);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    axios
      .get(
        `http://localhost:8000/get-cart_items_in_confirmed/${cardData.receipt_number}`
      )
      .then((response) => {
        console.log("Response", response.data.grouped_items);
        const cartItems = response.data.cart_items.map((item) => ({
          id: item.id,
          item_name: item.name,
          quantity: item.quantity,
        }));
        setData(cartItems);
        setMissingData(response.data.missing_items);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };

  // Check if cardData is empty
  if (!cardData) {
    return <p>No data available.</p>;
  }

  console.log("data:", data);

  return (
    <>
      <p>STATUS: {cardData.status}</p>
      <p>R#: {cardData.receipt_number}</p>

      {data.length > 0 ? (
        <>
          <h2>Grouped Items:</h2>
          <ul style={{ display: "flex", flexWrap: "wrap" }}>
            {data.map((item) => (
              <li
                style={{
                  border: "3px solid green",
                  borderRadius: "10px",
                  padding: "10px",
                  marginBottom: "10px",
                }}
                key={item.id}
              >
                <h1 style={{ marginRight: "10px" }}>
                  Item name: {item.item_name}
                </h1>
                <p>Qty: {item.quantity}</p>
              </li>
            ))}
          </ul>
        </>
      ) : (
        <>
          <h2>Line Items:</h2>
          <ul style={{ display: "flex", flexWrap: "wrap" }}>
            {line_items.map((item) => (
              <li
                style={{
                  border: "3px solid green",
                  borderRadius: "10px",
                  padding: "10px",
                  marginBottom: "10px",
                }}
                key={item.id}
              >
                <h1 style={{ marginRight: "10px" }}>
                  Item name: {item.item_name}
                </h1>
                <p>Qty: {item.quantity}</p>
              </li>
            ))}
          </ul>
        </>
      )}

      {missingData.length > 0 && (
        <>
          <h2>Missing Items:</h2>
          <ul style={{ display: "flex", flexWrap: "wrap" }}>
            {missingData.map((item) => (
              <li
                style={{
                  border: "3px solid red",
                  borderRadius: "10px",
                  padding: "10px",
                  marginBottom: "10px",
                }}
                key={item.id}
              >
                <h1 style={{ marginRight: "10px" }}>
                  Itemname: {item.item_name}
                </h1>
                <p>Qty: {item.quantity}</p>
              </li>
            ))}
          </ul>
        </>
      )}
    </>
  );
};

export default ConfirmedPage;
