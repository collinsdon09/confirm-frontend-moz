import { useState, useEffect } from "react";
import { SearchIcon } from "@chakra-ui/icons";
import {
  HStack,
  Input,
  InputGroup,
  InputRightElement,
  VStack,
} from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
} from "@chakra-ui/react";
import { Box } from "@chakra-ui/react";
import { connectSearchBox } from "react-instantsearch-core";
import { FaShoppingCart } from "react-icons/fa"; // Import the shopping cart icon from react-icons
import axios from "axios";
import CompareArrays from "../components/CompareArrays";
import _ from "lodash";

const SearchBox = connectSearchBox(
  ({ currentRefinement, refine, receipt_data }) => {
    const { isOpen, onOpen, onClose } = useDisclosure();

    const [receiptData, setReceiptData] = useState([]);
    const [cartname, setCartName] = useState("");
    const [comparisonResult, setComparisonResult] = useState([]);
    const [isButtonDisabled, setIsButtonDisabled] = useState(false);
    const [lineItems, setLineItems] = useState([]);
    const [cartItems, setCartItems] = useState([]);
    const [groupedItems, setGroupedItems] = useState([]);
    const [condition, setCondition] = useState(false);
    const [isConfirmDisabled, setIsConfirmDisabled] = useState(true);

    useEffect(() => {
      if (lineItems.length > 0 && cartItems.length > 0) {
        const isNameAvailable = lineItems.some((obj1) =>
          cartItems.some((obj2) => obj1.item_name === obj2.name)
        );
        const isAgeAvailable = lineItems.some((obj1) =>
          cartItems.some((obj2) => obj1.quantity === obj2.quantity)
        );
        setIsConfirmDisabled(!(isNameAvailable && isAgeAvailable));
      }
    }, [lineItems, cartItems]);

    const handleCartClick = () => {
      const namesArray = [];
      onOpen();
      setLineItems(receipt_data.line_items);


      const url = `http://localhost:8000/get-cart_items_in_receipt/${receipt_data.receipt_number}`;
      axios
        .get(url)
        .then((response) => {


          setCartItems(response.data.cart_items);
          //setGroupedItems(response.data.grouped_items);


          if (response.data.grouped_items) {
            setGroupedItems(response.data.grouped_items);
          } else {
            // Handle the case when response.data.grouped_items is undefined or null
            // You can set a default value or take appropriate action.
          }

          console.log(" from search g items", response.data.grouped_items);
          // const combinedArray = _.concat(groupedItems, response.data.grouped_items);
          //setGroupedItems(combinedArray);
          const hasRedBorder =
            Array.isArray(comparisonResult) &&
            comparisonResult.some((result) => !result.matched);
          setIsButtonDisabled(hasRedBorder);
          //const isGroupedItemsEmpty = response.data.grouped_items.length === 0;
          const isGroupedItemsEmpty = response.data.grouped_items && response.data.grouped_items.length === 0;

          setCondition(isGroupedItemsEmpty);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    };

    const handleTest = ()=> {
      console.log("cart is clicked!!!!!")
    }

    return (
      <>
        <HStack space={4}>
          <InputGroup>
            <InputRightElement
              pointerEvents="none"
              children={<SearchIcon color="brand.500" />}
            />
            <Input
              type="search"
              borderWidth={"1.5px"}
              borderColor={"brand.500"}
              placeholder="Search products"
              value={currentRefinement}
              onChange={(event) => refine(event.currentTarget.value)}
              borderRadius={"lg"}
            />
          </InputGroup>
          <FaShoppingCart ml={20} onClick={handleCartClick} />
          {/* <Button onClick={handleCartClick}>shop</Button> */}
        </HStack>

        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Cart Items</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <CompareArrays
                array1={lineItems}
                array2={cartItems}
                groupedItems={groupedItems}
                receipt_data_prop={receipt_data}
              />
            </ModalBody>
          </ModalContent>
        </Modal>
      </>
    );
  }
);

export default SearchBox;
