

import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

const AutoConfirmedPage = () => {
  const [updatedRAutoData, setUpdatedAutoData] = useState(null);

  const location = useLocation();
  const cardData = location.state;
  console.log("autconfirmed card clicked", cardData);

  // Check if cardData is empty
  if (!cardData) {
    return <p>No data available.</p>;
  }

  return (
    <>

      <p>STATUS: {cardData.status}</p>
      <p>R#: {cardData.receipt_number}</p>


      {cardData.grouped_items ? (
       
        <ul style={{ display: "flex", flexWrap: "wrap" }}>
          
          {cardData.grouped_items.map((item) => (
            <li
              style={{
                border: "3px solid green",
                borderRadius: "10px",
                padding: "10px",
                marginBottom: "10px",
              }}
              key={item.id}
            >
              <h1 style={{ marginRight: "10px" }}>Item name: {item.item_name}</h1>
              <p>Qty: {item.quantity}</p>
            </li>
          ))}
        </ul>
      ) : (
        <ul style={{ display: "flex", flexWrap: "wrap" }}>
          {cardData.line_items.map((item) => (
            <li
              style={{
                border: "3px solid green",
                borderRadius: "10px",
                padding: "10px",
                marginBottom: "10px",
              }}
              key={item.id}
            >
              <h1 style={{ marginRight: "10px" }}>Item name: {item.item_name}</h1>
              <p>Qty: {item.quantity}</p>
            </li>
          ))}
        </ul>
      )}
    </>
  );
};

export default AutoConfirmedPage;
