import { Stack } from "@chakra-ui/react";
import { InstantSearch } from "react-instantsearch-dom";
import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";
import CustomSearchBox from "../components/CustomSearchBox";
import Main from "../components/Main";
import Results from "../components/Results";
import { instantSearchConfig } from "../utils/typesense";

const INDEX_NAME = import.meta.env.VITE_APP_TYPESENSE_INDEX_NAME;
const typesenseInstantSearchAdapter = new TypesenseInstantSearchAdapter(
  instantSearchConfig
);

const Search_typesense = () => {
  return (
    <Main>
      <Stack
        w={"full"}
        justifyContent={"center"}
        alignItems={"center"}
        h={"auto"}
        pb={"2"}
        gap={"2"}
      >
        <InstantSearch
          indexName={INDEX_NAME}
          searchClient={typesenseInstantSearchAdapter.searchClient}
        >
          <CustomSearchBox />
          {/* <Results /> */}
        </InstantSearch>
      </Stack>
    </Main>
  );
};

export default Search_typesense;
