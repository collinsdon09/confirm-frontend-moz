import { useLocation } from "react-router-dom";
import { InstantSearch } from "react-instantsearch-core";
//import { instantSearchConfig } from "./typesense";
import { instantSearchConfig } from "../typesense";
import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";
import SearchBox from "./SearchBox";
import CustomComponent from "../components/Results";
import axios from "axios";
import { useEffect, useState } from "react";

const INDEX_NAME = import.meta.env.VITE_APP_TYPESENSE_INDEX_NAME;
const typesenseInstantSearchAdapter = new TypesenseInstantSearchAdapter(
  instantSearchConfig
);

import {
  Card,
  SimpleGrid,
  Button,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Stack,
  StackDivider,
  Box,
  Text,
  cookieStorageManager,
} from "@chakra-ui/react";

import SearchComponent from "../components/Search";

import { FaShoppingCart } from "react-icons/fa"; // Import the shopping cart icon from react-icons
import Results from "../components/Results";

// const fetchUpdatedReceiptwithGroupedItems = async (receiptNumber) => {
//   try {
//     const response = await axios.get(`http://localhost:8000/get-receipt-stats/${receiptNumber}`);
//     //setData(response.data);
//     console.log("updated Receipt from receipt view", response.data)
//   } catch (error) {
//     console.error("Error fetching data:", error);
//   }
// };

function ReceiptView() {
  const location = useLocation();
  const cardData = location.state;
  const [updatedReceiptData, setUpdatedReceiptData] = useState(null);
  //const customFunction = state.customFunction;

  //console.log("from receiptView:", cardData);

  const fetchUpdatedReceiptwithGroupedItems = async (receiptNumber) => {
   console.log("get function called....")
   
    try {
      const response = await axios.get(
        `http://localhost:8000/get-receipt-stats/${receiptNumber}`
      );

      // Do something with the response data

      console.log(
        // "Updated Receipt from receipt view",
        // response.data.cart_items
      );
      setUpdatedReceiptData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    //customFunction()

    fetchUpdatedReceiptwithGroupedItems(cardData.receipt_number);
  }, []);

  //console.log("UPDATED RECEIPT",fetchUpdatedReceiptwithGroupedItems(cardData.receipt_number))

  return (
    <>
      <p>You are now viewing Receipt#:- {cardData.receipt_number}</p>
      <InstantSearch
        indexName={INDEX_NAME}
        searchClient={typesenseInstantSearchAdapter.searchClient}
      >
        <SearchBox receipt_data={cardData} />

        <Results
          receipt_data={cardData}
          updated={updatedReceiptData}
          get={fetchUpdatedReceiptwithGroupedItems}
        />

        {/* <Results receipt_data={updatedReceiptData}/> */}
      </InstantSearch>
    </>
  );
}

export default ReceiptView;
