import { useState, useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import myStyle from "../myStyle/Tabs1.css"
import viteLogo from "/vite.svg";
// import "./App.css";
import { Heading, Text, Box, Flex, Spacer } from "@chakra-ui/react";
import axios from "axios";
// import { useHistory } from "react-router-dom";
import { FaShoppingCart } from "react-icons/fa";
import { Tabs, TabList, TabPanels, Tab, TabPanel } from "@chakra-ui/react";
import ToggleColorMode from "./ToggleColormode";
import ReceiptCard from "./ReceiptCard";
import WebSocketComponent from "./Websockets";
import Stats from "./Stats";
import {
  Card,
  SimpleGrid,
  Button,
  CardHeader,
  CardBody,
  CardFooter,
} from "@chakra-ui/react";
import { CalendarIcon, WarningIcon } from "@chakra-ui/icons";
function Tabz() {
  const [count, setCount] = useState(0);
  const [Opencards, setOpenCards] = useState([]);
  const [AutoConfirmedCards, setAutoConfirmedCards] = useState([]);
  const [Refundcards, setRefundCards] = useState([]);
  const [ConfirmedCards, setConfirmedCards] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    fetchOpenReceipts();
    fetchAutoConfirmedReceipts();
    fetchRefundReceipts();
    fetchConfirmedReceipts();
  }, []);

  const fetchData = async () => {
    console.log("fetch data is called...");
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-receipts"
      );
      const dataArray = Array.from(response.data);
      console.log(response.data);
      const receivedData = JSON.parse(response);
      console.log("res", receivedData);

      const filteredCards = dataArray.filter((item) => item.total_money > 100);
      console.log(filteredCards);

      setCards(filteredCards);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchOpenReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-receipts-type",
        {
          params: { status: "OPEN" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("res:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log("unique open:", uniqueArray);

      const filteredCards = uniqueArray.filter(
        (item) => item.total_money > 100
      );
      //console.log(filteredCards);
      setOpenCards(filteredCards);
    } catch (error) {
      console.error(error);
    }
  };

  const fetchRefundReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-receipts-refund",
        {
          params: { status: "REFUND" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("res:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log("unique open:", uniqueArray);

      const filteredCards = uniqueArray.filter(
        (item) => item.total_money > 100
      );
      //console.log(filteredCards);
      setRefundCards(filteredCards);
    } catch (error) {
      console.error(error);
    }
  };

  const fetchAutoConfirmedReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-receipts-type",
        {
          params: { status: "AUTO-CONFIRMED" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      //console.log("res:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log("unique auto-confirmed:", uniqueArray);

      const filteredCards = uniqueArray.filter(
        (item) => item.total_money > 100
      );
      //console.log(filteredCards);
      setAutoConfirmedCards(filteredCards);
    } catch (error) {
      console.error(error);
    }
  };

  /////////////////////////CONFIRMED RECEIPTS//////////////////////
  const fetchConfirmedReceipts = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8000/api/get-confirmed-receipts",
        {
          params: { status: "CONFIRMED" }, // Replace "OPEN" with the desired status value
        }
      );
      //setReceipts(response.data);
      console.log("confirmed receipts:", response.data);
      const dataArray = Array.from(response.data);

      //TO BE REMOVED ON PRODUCTION
      const uniqueArray = Object.values(
        dataArray.reduce((uniqueItems, currentItem) => {
          const key = currentItem.receipt_number; // Choose a unique property to identify duplicates

          if (!uniqueItems[key]) {
            uniqueItems[key] = currentItem;
          }

          return uniqueItems;
        }, {})
      );

      //console.log("unique confirmed:", uniqueArray);

      const filteredCards = uniqueArray.filter(
        (item) => item.total_money > 100
      );
      //console.log("filtered",filteredCards);
      setConfirmedCards(filteredCards);

      //console.log('confirmacio', ConfirmedCards)
    } catch (error) {
      console.error(error);
    }
  };

  const handleCardClick = (cardData) => {
    // Handle the card click event here
    //console.log("Card clicked:", cardData);
    // history.push(`/receipts/${cardData.receipt_number}`);
    navigate("/receipt", { state: cardData });
  };

  return (
    <>
      <WebSocketComponent />

      <ToggleColorMode />
      <Tabs variant="enclosed">
        <TabList>
          <Tab fontWeight="extrabold">To Be confirmed</Tab>
          <Tab fontWeight="extrabold">Auto-Confirmed</Tab>
          <Tab fontWeight="extrabold">Refund</Tab>
          <Tab fontWeight="extrabold">Confirmed</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <SimpleGrid
              spacing={14}
              templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
            >
              <Card>
                <CardHeader>
                  <Heading size="sm"> </Heading>
                </CardHeader>
                <CardBody>
                  <Text>customer: dfdfd</Text>
                  <Text>STATUS: dfdfd</Text>
                </CardBody>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}
                </CardFooter>
              </Card>
            </SimpleGrid>
          </TabPanel>
          <TabPanel>
            <p>Auto-confirmed Tab</p>
            <SimpleGrid
              spacing={14}
              templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
            >
              <Card>
                <CardHeader>
                  <Heading size="sm"> </Heading>
                </CardHeader>
                <CardBody>
                  <Text>customer: dfdfd</Text>
                  <Text>STATUS: dfdfd</Text>
                </CardBody>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}
                </CardFooter>
              </Card>
            </SimpleGrid>
          </TabPanel>

          <TabPanel>
            <p>Refund</p>

            <SimpleGrid
              spacing={14}
              templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
            >
              <Card>
                <CardHeader>
                  <Heading size="sm"> </Heading>
                </CardHeader>
                <CardBody>
                  <Text>customer: dfdfd</Text>
                  <Text>STATUS: dfdfd</Text>
                </CardBody>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}
                </CardFooter>
              </Card>
            </SimpleGrid>
          </TabPanel>

          <TabPanel>
            confirmed
            <SimpleGrid spacing={5} className="container">
              <Card className="card" rounded-lg>
                <CardHeader className="card-header">
                  <Heading size="md" className="Heading">
                    10-44er3432
                  </Heading>

                  <Text>14:30</Text>
                </CardHeader>
                <CardBody>
                  <Text>customer: Tom Tonny</Text>
                  <Text>STATUS: dfdfd</Text>
                </CardBody>
                <div className="status-infor">
                  <CalendarIcon color="	#0000ff" />
                  <Text>0/15</Text>
                </div>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}
                  <div className="card-header">
                    <Button
                      className="btn"
                      color="#FF0000"
                      bgColor="transparent"
                      fontWeight="800"
                      fontSize="xl"
                    >
                      <WarningIcon color="#FF0000" />
                      Open
                    </Button>
                  </div>
                </CardFooter>
              </Card>
              {/* *****************************************************************************8 */}
              <Card className="card" width="full">
                <CardHeader className="card-header">
                  <Heading size="md" className="Heading">
                    10-44er3432
                  </Heading>
                  <Text>14:30</Text>
                </CardHeader>
                <CardBody flex flexDirection="column">
                  <Text>customer: John WIck</Text>
                  <Text>STATUS: dfdfd</Text>
                </CardBody>
                <div className="status-infor">
                  <CalendarIcon color="	#0000ff" />
                  <Text>0/15</Text>
                </div>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}
                  <div className="card-header">
                    <Button
                      className="btn"
                      color="#37FD12"
                      bgColor="transparent"
                      fontWeight="800"
                      fontSize="xl"
                    >
                      <WarningIcon color="#37FD12" />
                      Progress
                    </Button>
                  </div>
                </CardFooter>
              </Card>
              <Card className="card">
                <CardHeader className="card-header">
                  <Heading size="md" className="Heading">
                    10-44er2
                  </Heading>
                  <Text>14:30</Text>
                </CardHeader>
                <CardBody>
                  <Text>customer: Tom Tonny</Text>
                  <Text>Wait: dfdfd</Text>
                </CardBody>
                <div className="status-infor">
                  <FaShoppingCart size={24}/>
                  <Text fontSize="lg" fontWeight="800">0/15</Text>
                </div>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}

                  <div className="card-header">
                    <Button
                      className="btn"
                      color="#FF0000"
                      bgColor="transparent"
                      fontWeight="800"
                      fontSize="xl"
                    >
                      <WarningIcon color="#FF0000" />
                      Open
                    </Button>
                  </div>
                </CardFooter>
              </Card>
              <Card className="card">
                <CardHeader className="card-header">
                  <Heading size="md" className="Heading">
                    10-44er3432
                  </Heading>
                  <Text>14:30</Text>
                </CardHeader>
                <CardBody flex flexDirection="column">
                  <Text>customer: John WIck</Text>
                  <Text>STATUS: dfdfd</Text>
                </CardBody>
                <div className="status-infor">
                  <CalendarIcon color="	#0000ff" />
                  <Text>0/15</Text>
                </div>
                <CardFooter>
                  {/* <Button onClick={() => handleCardClick(card)}>
                      View Receipt
                    </Button> */}
                  <div className="card-header">
                    <Button
                      className="btn"
                      color="#37FD12"
                      bgColor="transparent"
                      fontWeight="800"
                      fontSize="xl"
                    >
                      <WarningIcon color="#37FD12" />
                      Open
                    </Button>
                  </div>
                </CardFooter>
              </Card>
            </SimpleGrid>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  );
}

export default Tabz;
